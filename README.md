# Instagram but simpler

`./gradlew build` -> builds the project

`./gradlew test` -> run tests

`./gradlew assembleDebug` -> builds debug apk

## Libraries

Dagger 2 

Retrofit

Room

Glide

AutoValue

RxJava 2

...

## Architecture

MVP

Unidirectional dataflow

Hexagonal architecture

- `core` module contains pure JAVA code

- `android` module contains Android specifics


