package juanky.soriano.instagrambutsimpler.newphoto;

import android.view.View;

import dagger.Module;
import dagger.Provides;
import juanky.soriano.instagrambutsimpler.R;
import juanky.soriano.instagrambutsimpler.injection.PerActivity;
import juanky.soriano.instagrambutsimpler.newphoto.service.NewPhotoService;
import juanky.soriano.instagrambutsimpler.view.imageloader.ImageLoader;
import juanky.soriano.instagrambutsimpler.view.SnackbarDisplayer;

@PerActivity
@Module
public class NewPhotoActivityModule {

    private final NewPhotoActivity activity;

    public NewPhotoActivityModule(NewPhotoActivity activity) {
        this.activity = activity;
    }

    @Provides
    @PerActivity
    NewPhotoPresenter newPhotoPresenter(NewPhotoDisplayer displayer, NewPhotoService service) {
        return new NewPhotoPresenter(displayer, service);
    }

    @Provides
    @PerActivity
    SnackbarDisplayer snackbarDisplayer() {
        return new SnackbarDisplayer();
    }

    @Provides
    @PerActivity
    NewPhotoActivity newPhotoActivity() {
        return activity;
    }

    @Provides
    @PerActivity
    NewPhotoDisplayer newPhotoDisplayer(View newPhotoView, ImageLoader imageLoader, SnackbarDisplayer snackbarDisplayer) {
        return new AndroidNewPhotoDisplayer(newPhotoView, imageLoader, snackbarDisplayer);
    }

    @Provides
    @PerActivity
    View newPhotoView(NewPhotoActivity activity) {
        return activity.findViewById(R.id.new_photo);
    }
}
