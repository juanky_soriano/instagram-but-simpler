package juanky.soriano.instagrambutsimpler.newphoto;

import dagger.Subcomponent;
import juanky.soriano.instagrambutsimpler.domain.DomainModule;
import juanky.soriano.instagrambutsimpler.injection.PerActivity;
import juanky.soriano.instagrambutsimpler.newphoto.service.NewPhotoServiceModule;

@PerActivity
@Subcomponent(modules = {NewPhotoActivityModule.class, NewPhotoServiceModule.class, DomainModule.class})
public interface NewPhotoActivityComponent {

    void inject(NewPhotoActivity activity);
}
