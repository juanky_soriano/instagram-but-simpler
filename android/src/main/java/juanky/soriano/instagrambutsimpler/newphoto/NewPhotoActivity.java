package juanky.soriano.instagrambutsimpler.newphoto;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;

import javax.inject.Inject;
import java.net.URI;

import juanky.soriano.instagrambutsimpler.BuildConfig;
import juanky.soriano.instagrambutsimpler.InstagramButSimplerActivity;
import juanky.soriano.instagrambutsimpler.R;
import juanky.soriano.instagrambutsimpler.injection.Injector;
import juanky.soriano.instagrambutsimpler.service.CompletedListener;

public class NewPhotoActivity extends InstagramButSimplerActivity {

    private static final String ACTION = BuildConfig.APPLICATION_ID + ".NewPhoto";

    public static Intent createIntent(Uri photoUri) {
        return new Intent(ACTION).putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
    }

    @Inject
    NewPhotoPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_photo);
        Injector.inject(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Uri uri = getIntent().getParcelableExtra(MediaStore.EXTRA_OUTPUT);
        presenter.startPresenting(URI.create(uri.toString()), listener);
    }

    private final CompletedListener listener = new CompletedListener() {
        @Override
        public void onCompleted() {
            finish();
        }
    };

    @Override
    protected void onStop() {
        presenter.stopPresenting();
        super.onStop();
    }
}
