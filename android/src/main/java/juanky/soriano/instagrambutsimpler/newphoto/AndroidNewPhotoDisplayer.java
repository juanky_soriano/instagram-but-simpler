package juanky.soriano.instagrambutsimpler.newphoto;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import javax.inject.Inject;
import java.net.URI;
import java.util.Date;

import juanky.soriano.instagrambutsimpler.R;
import juanky.soriano.instagrambutsimpler.domain.Comment;
import juanky.soriano.instagrambutsimpler.domain.Image;
import juanky.soriano.instagrambutsimpler.domain.Photo;
import juanky.soriano.instagrambutsimpler.domain.PhotoId;
import juanky.soriano.instagrambutsimpler.domain.Title;
import juanky.soriano.instagrambutsimpler.view.imageloader.ImageLoader;
import juanky.soriano.instagrambutsimpler.view.SnackbarDisplayer;

class AndroidNewPhotoDisplayer implements NewPhotoDisplayer {
    private final View newPhotoView;
    private final ImageLoader imageLoader;
    private final SnackbarDisplayer snackbarDisplayer;

    private ActionListener listener = ActionListener.NO_OP;

    @Inject
    AndroidNewPhotoDisplayer(View newPhotoView, ImageLoader imageLoader, SnackbarDisplayer snackbarDisplayer) {
        this.newPhotoView = newPhotoView;
        this.imageLoader = imageLoader;
        this.snackbarDisplayer = snackbarDisplayer;
    }

    @Override
    public void attach(final ActionListener listener) {
        this.listener = listener;
    }

    @Override
    public void displayFor(URI photoUri) {
        ImageView photoImageView = newPhotoView.findViewById(R.id.photo_image);
        imageLoader.load(photoUri, photoImageView);
        newPhotoView.findViewById(R.id.new_photo_ok).setOnClickListener(onSendPhotoClicked(photoUri));
    }

    private View.OnClickListener onSendPhotoClicked(final URI uri) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Photo photo = createPhoto();
                listener.onNewPhoto(photo);
            }

            private Photo createPhoto() {
                String title = ((EditText) newPhotoView.findViewById(R.id.photo_title)).getText().toString();
                String comment = ((EditText) newPhotoView.findViewById(R.id.photo_comment)).getText().toString();
                return Photo.create(PhotoId.NOT_REQUIRED, Title.create(title), Image.create(uri), Comment.create(comment), new Date());
            }
        };
    }

    @Override
    public void displayErrorStoring() {
        snackbarDisplayer.show(newPhotoView, R.string.error_storing);
    }

    @Override
    public void detach() {
        listener = ActionListener.NO_OP;
    }
}
