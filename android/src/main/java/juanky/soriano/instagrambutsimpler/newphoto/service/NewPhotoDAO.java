package juanky.soriano.instagrambutsimpler.newphoto.service;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;

import juanky.soriano.instagrambutsimpler.domain.IOPhoto;

@Dao
public interface NewPhotoDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(IOPhoto photo);
}
