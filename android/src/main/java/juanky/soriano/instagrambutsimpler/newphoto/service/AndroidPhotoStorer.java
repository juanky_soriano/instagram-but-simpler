package juanky.soriano.instagrambutsimpler.newphoto.service;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import juanky.soriano.instagrambutsimpler.domain.Converter;
import juanky.soriano.instagrambutsimpler.domain.IOPhoto;
import juanky.soriano.instagrambutsimpler.domain.Photo;

class AndroidPhotoStorer implements PhotoStorer {

    private final NewPhotoDAO dao;
    private final Converter<Photo, IOPhoto> converter;

    @Inject
    AndroidPhotoStorer(NewPhotoDAO dao,
                       Converter<Photo, IOPhoto> converter) {
        this.dao = dao;
        this.converter = converter;
    }

    @Override
    public Completable store(final Photo photo) {
        return Observable.just(photo)
                .map(toIOPhoto())
                .flatMapCompletable(storeInDb());
    }

    private Function<Photo, IOPhoto> toIOPhoto() {
        return new Function<Photo, IOPhoto>() {
            @Override
            public IOPhoto apply(Photo photo) {
                return converter.from(photo);
            }
        };
    }

    private Function<IOPhoto, Completable> storeInDb() {
        return new Function<IOPhoto, Completable>() {

            @Override
            public Completable apply(IOPhoto ioPhoto) {
                dao.insert(ioPhoto);
                return Completable.complete();
            }
        };
    }
}
