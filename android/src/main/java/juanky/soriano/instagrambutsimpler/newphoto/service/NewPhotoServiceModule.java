package juanky.soriano.instagrambutsimpler.newphoto.service;

import dagger.Module;
import dagger.Provides;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import juanky.soriano.instagrambutsimpler.domain.Converter;
import juanky.soriano.instagrambutsimpler.service.InstagramButSimplerDatabase;
import juanky.soriano.instagrambutsimpler.domain.IOPhoto;
import juanky.soriano.instagrambutsimpler.domain.Photo;
import juanky.soriano.instagrambutsimpler.injection.PerActivity;
import juanky.soriano.instagrambutsimpler.support.RxExecutor;

@PerActivity
@Module
public class NewPhotoServiceModule {

    @Provides
    @PerActivity
    RxExecutor rxExecutor() {
        return RxExecutor.create(AndroidSchedulers.mainThread(), Schedulers.io());
    }

    @Provides
    @PerActivity
    NewPhotoDAO newPhotoDAO(InstagramButSimplerDatabase database) {
        return database.newPhotoDAO();
    }

    @Provides
    @PerActivity
    PhotoStorer photoStorer(NewPhotoDAO newPhotoDAO, Converter<Photo, IOPhoto> converter) {
        return new AndroidPhotoStorer(newPhotoDAO, converter);
    }
}
