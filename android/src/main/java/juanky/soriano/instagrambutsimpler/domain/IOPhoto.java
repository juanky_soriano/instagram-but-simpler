package juanky.soriano.instagrambutsimpler.domain;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

@Entity(tableName = "photo")
public class IOPhoto {

    @SerializedName("id")
    @Expose
    @PrimaryKey(autoGenerate = true)
    private Long id;
    @SerializedName("title")
    @Expose
    @ColumnInfo
    private String title;
    @SerializedName("publishedAt")
    @Expose
    @ColumnInfo
    private Date publishedAt;
    @SerializedName("photo")
    @Expose
    @ColumnInfo
    private String photoUri;
    @SerializedName("comment")
    @Expose
    @ColumnInfo
    private String comment;

    public String title() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date publishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(Date publishedAt) {
        this.publishedAt = publishedAt;
    }

    public String photoUri() {
        return photoUri;
    }

    public void setPhotoUri(String photoUri) {
        this.photoUri = photoUri;
    }

    public Long id() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String comment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        IOPhoto ioPhoto = (IOPhoto) o;

        return title.equals(ioPhoto.title)
                && publishedAt.equals(ioPhoto.publishedAt)
                && photoUri.equals(ioPhoto.photoUri)
                && comment.equals(ioPhoto.comment);

    }

    @Override
    public int hashCode() {
        int result = title.hashCode();
        result = 31 * result + publishedAt.hashCode();
        result = 31 * result + photoUri.hashCode();
        result = 31 * result + comment.hashCode();
        return result;
    }
}
