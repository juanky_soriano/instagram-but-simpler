package juanky.soriano.instagrambutsimpler.domain;

import java.net.URI;
import java.util.Date;

class PhotoConverter implements Converter<IOPhoto, Photo> {
    public Photo from(IOPhoto ioPhoto) {
        PhotoId id = PhotoId.create(ioPhoto.id());
        Title title = Title.create(ioPhoto.title());
        Image image = Image.create(URI.create(ioPhoto.photoUri()));
        Comment comment = Comment.create(ioPhoto.comment());
        Date date = ioPhoto.publishedAt();
        return Photo.create(id, title, image, comment, date);
    }
}
