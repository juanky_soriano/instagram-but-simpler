package juanky.soriano.instagrambutsimpler.domain;

import java.util.Date;

class IOPhotoConverter implements Converter<Photo, IOPhoto> {
    public IOPhoto from(Photo photo) {
        IOPhoto ioPhoto = new IOPhoto();
        if (photo.hasId()) {
            ioPhoto.setId(photo.id().rawId());
        }
        ioPhoto.setTitle(photo.title().rawTitle());
        ioPhoto.setComment(photo.comment().rawComment());
        ioPhoto.setPublishedAt(new Date(photo.date().getTime()));
        ioPhoto.setPhotoUri(photo.image().imageUri().toString());

        return ioPhoto;
    }
}
