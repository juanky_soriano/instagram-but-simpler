package juanky.soriano.instagrambutsimpler.domain;

import dagger.Module;
import dagger.Provides;
import juanky.soriano.instagrambutsimpler.injection.PerActivity;

@PerActivity
@Module
public class DomainModule {
    @Provides
    @PerActivity
    Converter<IOPhoto, Photo> photoConverter() {
        return new PhotoConverter();
    }

    @Provides
    @PerActivity
    Converter<Photo, IOPhoto> ipPhotoConverter() {
        return new IOPhotoConverter();
    }
}
