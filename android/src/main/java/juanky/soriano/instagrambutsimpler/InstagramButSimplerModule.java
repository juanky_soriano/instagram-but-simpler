package juanky.soriano.instagrambutsimpler;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.res.Resources;
import android.util.DisplayMetrics;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import dagger.Module;
import dagger.Provides;
import juanky.soriano.instagrambutsimpler.injection.PerApplication;
import juanky.soriano.instagrambutsimpler.service.InstagramButSimplerDatabase;
import juanky.soriano.instagrambutsimpler.support.DeviceForm;
import juanky.soriano.instagrambutsimpler.view.imageloader.ImageLoader;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@PerApplication
@Module
public class InstagramButSimplerModule {

    private static final String BASE_URL = "https://photomaton.herokuapp.com/";
    private static final int CACHE_SIZE = 24 * 1024 * 1024;
    private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSz";

    private final Application application;

    public InstagramButSimplerModule(Application application) {
        this.application = application;
    }

    @Provides
    @PerApplication
    Application application() {
        return application;
    }

    @Provides
    @PerApplication
    Gson gson() {
        return new GsonBuilder()
                .setDateFormat(DATE_FORMAT)
                .create();
    }

    @Provides
    @PerApplication
    Cache cache(Application application) {
        return new Cache(application.getCacheDir(), CACHE_SIZE);
    }

    @Provides
    @PerApplication
    OkHttpClient okHttpClient(Cache cache) {
        return new OkHttpClient.Builder()
                .cache(cache)
                .build();
    }

    @Provides
    @PerApplication
    Retrofit retrofit(Gson gson, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .build();
    }

    @Provides
    @PerApplication
    InstagramButSimplerDatabase database(Application application) {
        return Room.databaseBuilder(application, InstagramButSimplerDatabase.class, "instagram-but-simpler").build();
    }

    @Provides
    @PerApplication
    ImageLoader imageLoader(RequestManager requestManager, DisplayMetrics displayMetrics) {
        return new ImageLoader(requestManager, displayMetrics);
    }

    @Provides
    @PerApplication
    RequestManager requestManager(Application application) {
        return Glide.with(application);
    }

    @Provides
    @PerApplication
    DisplayMetrics displayMetrics() {
        return Resources.getSystem().getDisplayMetrics();
    }

    @Provides
    @PerApplication
    DeviceForm deviceForm(Resources resources) {
        return new DeviceForm(resources);
    }

    @Provides
    @PerApplication
    Resources resources(Application application) {
        return application.getResources();
    }
}
