package juanky.soriano.instagrambutsimpler;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory;
import com.bumptech.glide.load.engine.cache.LruResourceCache;
import com.bumptech.glide.module.GlideModule;

public class InstagramButSimplerGlideModule implements GlideModule {

    private static final int CACHE_SIZE = 24 * 1024 * 1024;

    @Override
    public void applyOptions(Context context, GlideBuilder builder) {
        builder.setDiskCache(new InternalCacheDiskCacheFactory(context, CACHE_SIZE));
        builder.setMemoryCache(new LruResourceCache(CACHE_SIZE));

    }

    @Override
    public void registerComponents(Context context, Glide glide) {
        //no-op
    }
}
