package juanky.soriano.instagrambutsimpler.navigation;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;

import juanky.soriano.instagrambutsimpler.R;
import juanky.soriano.instagrambutsimpler.domain.Photo;
import juanky.soriano.instagrambutsimpler.newphoto.NewPhotoActivity;
import juanky.soriano.instagrambutsimpler.photodetails.PhotoDetailsActivity;
import juanky.soriano.instagrambutsimpler.support.FileUtils;

public class AndroidNavigator implements Navigator {
    private final Activity activity;

    @Inject
    public AndroidNavigator(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void toDetailsOf(Photo photo, Transitable transitable) {
        Bundle bundle = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, transitable.transitAs(View.class), transitable.name()).toBundle();
        Intent intent = PhotoDetailsActivity.createIntent(photo.id());

        ActivityCompat.startActivity(activity, intent, bundle);
    }

    @Override
    public void toTakePhoto(int requestCode, File photoFile) {
        Uri uri = FileUtils.getUriForFile(activity, photoFile);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE).putExtra(MediaStore.EXTRA_OUTPUT, uri);

        activity.startActivityForResult(intent, requestCode);
    }

    @Override
    public void toNewPhoto(File newPhotoFile) {
        Uri uri = FileUtils.getUriForFile(activity, newPhotoFile);
        Intent intent = NewPhotoActivity.createIntent(uri);

        activity.startActivity(intent);
    }

    @Override
    public void toSharePhoto(Shareable shareable) {
        try {
            Uri photoBitmapUri = FileUtils.getUriForBitmap(activity, shareable.shareAs(Bitmap.class));
            Intent intent = new Intent(Intent.ACTION_SEND)
                    .setType("image/*")
                    .putExtra(Intent.EXTRA_STREAM, photoBitmapUri);
            String chooserMessage = activity.getString(R.string.share_with_friends);
            activity.startActivity(Intent.createChooser(intent, chooserMessage));
        } catch (IOException exception) {
            Log.e(getClass().getCanonicalName(), "Error sharing photo", exception);
            Toast.makeText(activity, R.string.error_sharing, Toast.LENGTH_SHORT).show();
        }
    }

}
