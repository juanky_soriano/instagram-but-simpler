package juanky.soriano.instagrambutsimpler.photodetails;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import dagger.Module;
import dagger.Provides;
import juanky.soriano.instagrambutsimpler.R;
import juanky.soriano.instagrambutsimpler.injection.PerActivity;
import juanky.soriano.instagrambutsimpler.navigation.AndroidNavigator;
import juanky.soriano.instagrambutsimpler.navigation.Navigator;
import juanky.soriano.instagrambutsimpler.navigation.Shareable;
import juanky.soriano.instagrambutsimpler.photodetails.service.PhotoService;
import juanky.soriano.instagrambutsimpler.view.PhotoView;
import juanky.soriano.instagrambutsimpler.view.SnackbarDisplayer;

@PerActivity
@Module
public class PhotoDetailsActivityModule {

    private final PhotoDetailsActivity activity;

    public PhotoDetailsActivityModule(PhotoDetailsActivity activity) {
        this.activity = activity;
    }

    @Provides
    @PerActivity
    PhotoDetailsPresenter photoDetailsPresenter(PhotoDetailsDisplayer displayer, PhotoService service) {
        return new PhotoDetailsPresenter(displayer, service);
    }

    @Provides
    @PerActivity
    SnackbarDisplayer snackbarDisplayer() {
        return new SnackbarDisplayer();
    }

    @Provides
    @PerActivity
    PhotoDetailsActivity photoDetailsActivity() {
        return activity;
    }

    @Provides
    @PerActivity
    PhotoView photoView(PhotoDetailsActivity activity) {
        return (PhotoView) activity.findViewById(R.id.photo_details);
    }

    @Provides
    @PerActivity
    PhotoDetailsDisplayer photoDetailsDisplayer(PhotoView photoView, SnackbarDisplayer snackbarDisplayer) {
        return new AndroidPhotoDetailsDisplayer(photoView, snackbarDisplayer);
    }

    @Provides
    @PerActivity
    Navigator navigator(PhotoDetailsActivity activity) {
        return new AndroidNavigator(activity);
    }

    @Provides
    @PerActivity
    Shareable shareable(PhotoView photoView) {
        return photoView;
    }

    @Provides
    @PerActivity
    Executor executor() {
        return Executors.newSingleThreadExecutor();
    }
}
