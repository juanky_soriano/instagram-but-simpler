package juanky.soriano.instagrambutsimpler.photodetails;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import javax.inject.Inject;
import java.util.concurrent.Executor;

import dagger.Lazy;
import juanky.soriano.instagrambutsimpler.BuildConfig;
import juanky.soriano.instagrambutsimpler.InstagramButSimplerActivity;
import juanky.soriano.instagrambutsimpler.R;
import juanky.soriano.instagrambutsimpler.domain.Photo;
import juanky.soriano.instagrambutsimpler.domain.PhotoId;
import juanky.soriano.instagrambutsimpler.injection.Injector;
import juanky.soriano.instagrambutsimpler.navigation.Navigator;
import juanky.soriano.instagrambutsimpler.navigation.Shareable;
import juanky.soriano.instagrambutsimpler.service.LoadedListener;

public class PhotoDetailsActivity extends InstagramButSimplerActivity {

    private static final String ACTION = BuildConfig.APPLICATION_ID + ".PhotoDetails";
    private static final String EXTRA_PHOTO_ID = "photo_id";

    public static Intent createIntent(PhotoId id) {
        return new Intent(ACTION).putExtra(EXTRA_PHOTO_ID, id.rawId());
    }

    @Inject
    PhotoDetailsPresenter presenter;
    @Inject
    Navigator navigator;
    @Inject
    Lazy<Shareable> shareable;
    @Inject
    Executor executor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_details);
        Injector.inject(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.startPresenting(photoIdFromExtra(), photoLoadedListener);
    }

    private PhotoId photoIdFromExtra() {
        long rawId = getIntent().getLongExtra(EXTRA_PHOTO_ID, 0);
        return PhotoId.create(rawId);
    }

    private final LoadedListener<Photo> photoLoadedListener = new LoadedListener<Photo>() {
        @Override
        public void onLoaded(Photo loadedPhoto) {
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.share_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                supportFinishAfterTransition();
                return true;
            case R.id.menu_item_share:
                executor.execute(shareTask);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private final Runnable shareTask = new Runnable() {
        @Override
        public void run() {
            navigator.toSharePhoto(shareable.get());
        }
    };

    @Override
    protected void onStop() {
        presenter.stopPresenting();
        super.onStop();
    }
}
