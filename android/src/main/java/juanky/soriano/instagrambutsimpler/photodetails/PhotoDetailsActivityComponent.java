package juanky.soriano.instagrambutsimpler.photodetails;

import dagger.Subcomponent;
import juanky.soriano.instagrambutsimpler.domain.DomainModule;
import juanky.soriano.instagrambutsimpler.injection.PerActivity;
import juanky.soriano.instagrambutsimpler.photodetails.service.PhotoDetailsServiceModule;

@PerActivity
@Subcomponent(modules = {PhotoDetailsActivityModule.class, PhotoDetailsServiceModule.class, DomainModule.class})
public interface PhotoDetailsActivityComponent {

    void inject(PhotoDetailsActivity activity);
}
