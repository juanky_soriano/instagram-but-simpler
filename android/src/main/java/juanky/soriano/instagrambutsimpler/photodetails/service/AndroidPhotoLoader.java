package juanky.soriano.instagrambutsimpler.photodetails.service;

import javax.inject.Inject;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import juanky.soriano.instagrambutsimpler.domain.IOPhoto;
import juanky.soriano.instagrambutsimpler.domain.Photo;
import juanky.soriano.instagrambutsimpler.domain.PhotoId;
import juanky.soriano.instagrambutsimpler.domain.Converter;

class AndroidPhotoLoader implements PhotoLoader {

    private final PhotoDAO dao;
    private final Converter<IOPhoto, Photo> converter;

    @Inject
    AndroidPhotoLoader(PhotoDAO dao, Converter<IOPhoto, Photo> converter) {
        this.dao = dao;
        this.converter = converter;
    }

    @Override
    public Observable<Photo> loadPhoto(final PhotoId photoId) {
        return loadFromDb(photoId)
                .map(toPhoto());
    }

    private Observable<IOPhoto> loadFromDb(final PhotoId photoId) {
        return Observable.fromCallable(new Callable<IOPhoto>() {
            @Override
            public IOPhoto call() {
                return dao.retrieveById(photoId.rawId());
            }
        });
    }

    private Function<IOPhoto, Photo> toPhoto() {
        return new Function<IOPhoto, Photo>() {
            @Override
            public Photo apply(IOPhoto ioPhoto) {
                return converter.from(ioPhoto);
            }
        };
    }

}
