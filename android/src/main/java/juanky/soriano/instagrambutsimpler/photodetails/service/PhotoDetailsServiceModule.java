package juanky.soriano.instagrambutsimpler.photodetails.service;

import dagger.Module;
import dagger.Provides;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import juanky.soriano.instagrambutsimpler.service.InstagramButSimplerDatabase;
import juanky.soriano.instagrambutsimpler.domain.IOPhoto;
import juanky.soriano.instagrambutsimpler.domain.Photo;
import juanky.soriano.instagrambutsimpler.domain.Converter;
import juanky.soriano.instagrambutsimpler.injection.PerActivity;
import juanky.soriano.instagrambutsimpler.support.RxExecutor;

@PerActivity
@Module
public class PhotoDetailsServiceModule {

    @Provides
    @PerActivity
    RxExecutor rxExecutor() {
        return RxExecutor.create(AndroidSchedulers.mainThread(), Schedulers.io());
    }

    @Provides
    @PerActivity
    PhotoDAO photoDao(InstagramButSimplerDatabase database) {
        return database.photoDao();
    }

    @Provides
    @PerActivity
    PhotoLoader photoLoader(PhotoDAO photoDAO, Converter<IOPhoto, Photo> converter) {
        return new AndroidPhotoLoader(photoDAO, converter);
    }
}
