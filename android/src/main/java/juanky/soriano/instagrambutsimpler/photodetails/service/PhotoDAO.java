package juanky.soriano.instagrambutsimpler.photodetails.service;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import juanky.soriano.instagrambutsimpler.domain.IOPhoto;

@Dao
public interface PhotoDAO {

    @Query("SELECT * FROM photo WHERE id = :id")
    IOPhoto retrieveById(Long id);
}
