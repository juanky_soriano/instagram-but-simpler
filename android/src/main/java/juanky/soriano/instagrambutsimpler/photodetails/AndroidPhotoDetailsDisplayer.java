package juanky.soriano.instagrambutsimpler.photodetails;

import javax.inject.Inject;

import juanky.soriano.instagrambutsimpler.R;
import juanky.soriano.instagrambutsimpler.domain.Photo;
import juanky.soriano.instagrambutsimpler.view.PhotoActionListener;
import juanky.soriano.instagrambutsimpler.view.PhotoView;
import juanky.soriano.instagrambutsimpler.view.SnackbarDisplayer;

class AndroidPhotoDetailsDisplayer implements PhotoDetailsDisplayer {
    private final PhotoView photoView;
    private final SnackbarDisplayer snackbarDisplayer;
    private PhotoDetailsDisplayer.ActionListener listener = ActionListener.NO_OP;

    @Inject
    AndroidPhotoDetailsDisplayer(PhotoView photoView, SnackbarDisplayer snackbarDisplayer) {
        this.photoView = photoView;
        this.snackbarDisplayer = snackbarDisplayer;
    }

    @Override
    public void display(Photo photo) {
        photoView.updateWith(photo, PhotoActionListener.NO_OP);
    }

    @Override
    public void displayErrorLoading() {
        snackbarDisplayer.show(photoView, R.string.error_loading);
    }
}
