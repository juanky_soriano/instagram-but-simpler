package juanky.soriano.instagrambutsimpler.support;

import android.content.res.Resources;

import javax.inject.Inject;

import juanky.soriano.instagrambutsimpler.R;

public class DeviceForm {
    private final Resources resources;

    @Inject
    public DeviceForm(Resources resources) {
        this.resources = resources;
    }

    public boolean isTablet() {
        return resources.getBoolean(R.bool.isTablet);
    }

    public boolean isPhone() {
        return !isTablet();
    }
}
