package juanky.soriano.instagrambutsimpler.support;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.FileProvider;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

public abstract class FileUtils {
    public static Uri getUriForFile(Context context, File file) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return FileProvider.getUriForFile(context, "juanky.soriano.instagrambutsimpler.fileprovider", file);
        } else {
            return Uri.fromFile(file);
        }
    }

    public static File createImageFile(Context context) {
        String imageFileName = "InstagramButSimpler_" + UUID.randomUUID() + ".jpg";
        File filesDir = isExternalPicturesDirAvailable(context) ? context.getExternalFilesDir(Environment.DIRECTORY_PICTURES) : context.getFilesDir();
        return new File(filesDir, imageFileName);
    }

    private static boolean isExternalPicturesDirAvailable(Context context) {
        return context.getExternalFilesDir(Environment.DIRECTORY_PICTURES) != null;
    }

    public static Uri getUriForBitmap(Context context, Bitmap bitmap) throws IOException {
        File file = createImageFile(context);
        FileOutputStream out = new FileOutputStream(file);
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
        out.flush();
        out.close();
        return FileUtils.getUriForFile(context.getApplicationContext(), file);
    }
}
