package juanky.soriano.instagrambutsimpler.service;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverter;
import android.arch.persistence.room.TypeConverters;

import java.util.Date;

import juanky.soriano.instagrambutsimpler.domain.IOPhoto;
import juanky.soriano.instagrambutsimpler.gallery.service.GalleryDAO;
import juanky.soriano.instagrambutsimpler.newphoto.service.NewPhotoDAO;
import juanky.soriano.instagrambutsimpler.photodetails.service.PhotoDAO;

@Database(entities = {IOPhoto.class}, version = 1, exportSchema = false)
@TypeConverters({InstagramButSimplerDatabase.DatabaseConverters.class})
public abstract class InstagramButSimplerDatabase extends RoomDatabase {
    public abstract GalleryDAO galleryDao();
    public abstract PhotoDAO photoDao();
    public abstract NewPhotoDAO newPhotoDAO();

    public static class DatabaseConverters {
        @TypeConverter
        public static Date fromTimestamp(Long value) {
            return value == null ? null : new Date(value);
        }

        @TypeConverter
        public static Long dateToTimestamp(Date date) {
            return date == null ? null : date.getTime();
        }
    }
}
