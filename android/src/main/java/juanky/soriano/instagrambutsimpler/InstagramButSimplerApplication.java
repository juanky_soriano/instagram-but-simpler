package juanky.soriano.instagrambutsimpler;

import android.app.Application;

import juanky.soriano.instagrambutsimpler.injection.Injector;

public class InstagramButSimplerApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Injector.init(this);
    }
}
