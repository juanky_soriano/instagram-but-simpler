package juanky.soriano.instagrambutsimpler.gallery.service;

import java.util.List;

import io.reactivex.Observable;
import juanky.soriano.instagrambutsimpler.domain.IOPhoto;
import retrofit2.http.GET;

public interface GalleryAPI{
    @GET("api/photo/")
    Observable<List<IOPhoto>> load();
}
