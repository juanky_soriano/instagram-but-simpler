package juanky.soriano.instagrambutsimpler.gallery.service;

import javax.inject.Inject;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import juanky.soriano.instagrambutsimpler.domain.IOPhoto;
import juanky.soriano.instagrambutsimpler.support.StateChangeService;
import juanky.soriano.instagrambutsimpler.support.Status;

class AndroidGalleryUpdater implements GalleryUpdater {

    private final GalleryAPI galleryAPI;
    private final GalleryDAO galleryDAO;
    private final StateChangeService<Status> stateChangeService;

    @Inject
    AndroidGalleryUpdater(GalleryAPI galleryAPI,
                          GalleryDAO galleryDAO,
                          StateChangeService<Status> stateChangeService) {
        this.galleryAPI = galleryAPI;
        this.galleryDAO = galleryDAO;
        this.stateChangeService = stateChangeService;
    }

    @Override
    public Completable update() {
        return galleryAPI.load()
                .flatMapCompletable(insertInDb())
                .doOnSubscribe(notifyLoading())
                .doOnError(notifyError())
                .doOnComplete(notifyIdle());
    }

    private Function<List<IOPhoto>, Completable> insertInDb() {
        return new Function<List<IOPhoto>, Completable>() {
            @Override
            public Completable apply(List<IOPhoto> ioPhotos) {
                galleryDAO.insert(ioPhotos);
                return Completable.complete();
            }
        };
    }

    private Consumer<Disposable> notifyLoading() {
        return new Consumer<Disposable>() {
            @Override
            public void accept(Disposable disposable) {
                stateChangeService.notify(Status.LOADING);
            }
        };
    }

    private Consumer<Throwable> notifyError() {
        return new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) {
                stateChangeService.notify(Status.ERROR);
            }
        };
    }

    private Action notifyIdle() {
        return new Action() {
            @Override
            public void run() {
                stateChangeService.notify(Status.IDLE);
            }
        };
    }

}
