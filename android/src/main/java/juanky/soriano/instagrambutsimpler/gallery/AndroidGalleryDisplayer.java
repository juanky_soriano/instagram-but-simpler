package juanky.soriano.instagrambutsimpler.gallery;

import android.support.v4.widget.SwipeRefreshLayout;

import javax.inject.Inject;

import juanky.soriano.instagrambutsimpler.R;
import juanky.soriano.instagrambutsimpler.domain.Gallery;
import juanky.soriano.instagrambutsimpler.gallery.view.GalleryView;
import juanky.soriano.instagrambutsimpler.view.SnackbarDisplayer;

class AndroidGalleryDisplayer implements GalleryDisplayer {
    private final SwipeRefreshLayout refreshLayout;
    private final GalleryView galleryView;
    private final SnackbarDisplayer snackbarDisplayer;

    private ActionListener actionListener = ActionListener.NO_OP;

    @Inject
    AndroidGalleryDisplayer(SwipeRefreshLayout refreshView, GalleryView galleryView, SnackbarDisplayer snackbarDisplayer) {
        this.refreshLayout = refreshView;
        this.galleryView = galleryView;
        this.snackbarDisplayer = snackbarDisplayer;
    }

    @Override
    public void attach(ActionListener listener) {
        this.actionListener = listener;
        this.refreshLayout.setOnRefreshListener(refreshListener);
    }

    private final SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            actionListener.onForceRefresh();
        }
    };

    @Override
    public void displayLoading() {
        refreshLayout.setRefreshing(true);
    }

    @Override
    public void display(Gallery gallery) {
        galleryView.updateWith(gallery, actionListener);
    }

    @Override
    public void displayLoaded() {
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void displayErrorUpdating() {
        snackbarDisplayer.show(galleryView, R.string.error_updating);
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void displayErrorLoading() {
        snackbarDisplayer.show(galleryView, R.string.error_loading);
    }

    @Override
    public void detach() {
        this.actionListener = ActionListener.NO_OP;
        this.refreshLayout.setOnRefreshListener(null);
    }
}
