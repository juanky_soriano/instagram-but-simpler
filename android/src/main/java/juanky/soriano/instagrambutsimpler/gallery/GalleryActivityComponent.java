package juanky.soriano.instagrambutsimpler.gallery;

import dagger.Subcomponent;
import juanky.soriano.instagrambutsimpler.domain.DomainModule;
import juanky.soriano.instagrambutsimpler.gallery.service.GalleryServiceModule;
import juanky.soriano.instagrambutsimpler.injection.PerActivity;

@PerActivity
@Subcomponent(modules = {GalleryActivityModule.class, GalleryServiceModule.class, DomainModule.class})
public interface GalleryActivityComponent {

    void inject(GalleryActivity activity);
}
