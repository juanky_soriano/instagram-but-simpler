package juanky.soriano.instagrambutsimpler.gallery.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.List;

import juanky.soriano.instagrambutsimpler.R;
import juanky.soriano.instagrambutsimpler.domain.Photo;
import juanky.soriano.instagrambutsimpler.gallery.GalleryDisplayer;
import juanky.soriano.instagrambutsimpler.view.PhotoView;

class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {

    private List<Photo> photos = Collections.emptyList();
    private GalleryDisplayer.ActionListener listener = GalleryDisplayer.ActionListener.NO_OP;

    static GalleryAdapter newInstance() {
        GalleryAdapter adapter = new GalleryAdapter();
        adapter.setHasStableIds(true);
        return adapter;
    }

    private GalleryAdapter() {
        //no op
    }

    void update(List<Photo> newPhotos, GalleryDisplayer.ActionListener listener) {
        this.listener = listener;
        if (photos.equals(newPhotos)) {
            return;
        }

        photos = Collections.unmodifiableList(newPhotos);
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return photos.get(position).id().rawId();
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        PhotoView photoView = (PhotoView) layoutInflater.inflate(R.layout.photo_in_gallery_layout, parent, false);
        return new ViewHolder(photoView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Photo photo = photos.get(position);
        holder.bind(photo, listener);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        ViewHolder(PhotoView photoView) {
            super(photoView);
        }

        void bind(Photo photo, GalleryDisplayer.ActionListener listener) {
            PhotoView photoView = (PhotoView) itemView;
            photoView.updateWith(photo, listener);
        }
    }
}
