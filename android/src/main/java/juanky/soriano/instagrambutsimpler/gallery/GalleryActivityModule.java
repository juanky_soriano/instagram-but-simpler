package juanky.soriano.instagrambutsimpler.gallery;

import android.support.v4.widget.SwipeRefreshLayout;

import dagger.Module;
import dagger.Provides;
import juanky.soriano.instagrambutsimpler.navigation.AndroidNavigator;
import juanky.soriano.instagrambutsimpler.navigation.Navigator;
import juanky.soriano.instagrambutsimpler.R;
import juanky.soriano.instagrambutsimpler.gallery.service.GalleryService;
import juanky.soriano.instagrambutsimpler.gallery.view.GalleryView;
import juanky.soriano.instagrambutsimpler.injection.PerActivity;
import juanky.soriano.instagrambutsimpler.view.SnackbarDisplayer;
import juanky.soriano.instagrambutsimpler.view.imageloader.ImageLoader;

@PerActivity
@Module
public class GalleryActivityModule {

    private final GalleryActivity activity;

    public GalleryActivityModule(GalleryActivity activity) {
        this.activity = activity;
    }

    @Provides
    @PerActivity
    GalleryPresenter galleryPresenter(GalleryDisplayer galleryDisplayer, GalleryService service, Navigator navigator) {
        return new GalleryPresenter(galleryDisplayer, service, navigator);
    }

    @Provides
    @PerActivity
    SnackbarDisplayer snackbarDisplayer() {
        return new SnackbarDisplayer();
    }

    @Provides
    @PerActivity
    GalleryActivity galleryActivity() {
        return activity;
    }

    @Provides
    @PerActivity
    GalleryView galleryView(GalleryActivity galleryActivity) {
        return (GalleryView) galleryActivity.findViewById(R.id.photo_gallery);
    }


    @Provides
    @PerActivity
    SwipeRefreshLayout refreshView(GalleryActivity galleryActivity) {
        SwipeRefreshLayout refreshView = (SwipeRefreshLayout) galleryActivity.findViewById(R.id.refresh_layout);
        refreshView.setColorSchemeResources(R.color.colorAccent, R.color.colorPrimary);
        return refreshView;
    }

    @Provides
    @PerActivity
    GalleryDisplayer galleryDisplayer(SwipeRefreshLayout swipeRefreshLayout, GalleryView galleryView, SnackbarDisplayer snackbarDisplayer) {
        return new AndroidGalleryDisplayer(swipeRefreshLayout, galleryView, snackbarDisplayer);
    }

    @Provides
    @PerActivity
    Navigator navigator(GalleryActivity activity, ImageLoader imageLoader) {
        return new AndroidNavigator(activity);
    }
}
