package juanky.soriano.instagrambutsimpler.gallery.service;

import javax.inject.Inject;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import juanky.soriano.instagrambutsimpler.domain.Converter;
import juanky.soriano.instagrambutsimpler.domain.Gallery;
import juanky.soriano.instagrambutsimpler.domain.IOPhoto;
import juanky.soriano.instagrambutsimpler.domain.Photo;
import juanky.soriano.instagrambutsimpler.support.StateChangeService;
import juanky.soriano.instagrambutsimpler.support.Status;

class AndroidGalleryLoader implements GalleryLoader {

    private final GalleryDAO dao;
    private final Converter<IOPhoto, Photo> converter;
    private final StateChangeService<Status> stateChangeService;

    @Inject
    AndroidGalleryLoader(GalleryDAO dao, Converter<IOPhoto, Photo> converter, StateChangeService<Status> stateChangeService) {
        this.dao = dao;
        this.converter = converter;
        this.stateChangeService = stateChangeService;
    }

    @Override
    public Observable<Gallery> loadGallery() {
        return stateChangeService.observeStateChanges(Status.ERROR, Status.IDLE)
                .startWith(Status.LOADING)
                .flatMap(loadFromDb());
    }

    private Function<Status, Observable<Gallery>> loadFromDb() {
        return new Function<Status, Observable<Gallery>>() {
            @Override
            public Observable<Gallery> apply(Status status) {
                return Observable.just(dao.load())
                        .flatMap(toSequence())
                        .map(toPhoto())
                        .toList()
                        .map(toGallery())
                        .toObservable();
            }
        };
    }

    private Function<List<IOPhoto>, Observable<IOPhoto>> toSequence() {
        return new Function<List<IOPhoto>, Observable<IOPhoto>>() {
            @Override
            public Observable<IOPhoto> apply(List<IOPhoto> ioPhotos) {
                return Observable.fromIterable(ioPhotos);
            }
        };
    }

    private Function<IOPhoto, Photo> toPhoto() {
        return new Function<IOPhoto, Photo>() {
            @Override
            public Photo apply(IOPhoto ioPhoto) {
                return converter.from(ioPhoto);
            }
        };
    }

    private Function<List<Photo>, Gallery> toGallery() {
        return new Function<List<Photo>, Gallery>() {
            @Override
            public Gallery apply(List<Photo> photos) {
                return Gallery.create(photos);
            }
        };
    }
}
