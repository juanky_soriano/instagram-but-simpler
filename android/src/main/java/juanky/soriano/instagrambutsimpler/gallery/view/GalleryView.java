package juanky.soriano.instagrambutsimpler.gallery.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import javax.inject.Inject;

import juanky.soriano.instagrambutsimpler.domain.Gallery;
import juanky.soriano.instagrambutsimpler.gallery.GalleryDisplayer;
import juanky.soriano.instagrambutsimpler.injection.Injector;

public class GalleryView extends RecyclerView {

    private static final int TOP = 0;

    @Inject
    GalleryAdapter adapter;
    @Inject
    LayoutManager layoutManager;

    public GalleryView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Injector.inject(this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        setAdapter(adapter);
        setLayoutManager(layoutManager);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        adapter.registerAdapterDataObserver(observer);
    }

    private final AdapterDataObserver observer = new AdapterDataObserver() {
        @Override
        public void onChanged() {
            smoothScrollToPosition(TOP);
        }
    };

    public void updateWith(Gallery gallery, GalleryDisplayer.ActionListener listener) {
        adapter.update(gallery.photos(), listener);
    }

    @Override
    protected void onDetachedFromWindow() {
        adapter.unregisterAdapterDataObserver(observer);
        super.onDetachedFromWindow();
    }
}
