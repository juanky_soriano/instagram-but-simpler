package juanky.soriano.instagrambutsimpler.gallery.service;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import juanky.soriano.instagrambutsimpler.domain.IOPhoto;

@Dao
public interface GalleryDAO {

    @Query("SELECT * FROM photo ORDER BY id DESC")
    List<IOPhoto> load();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<IOPhoto> photos);
}
