package juanky.soriano.instagrambutsimpler.gallery.service;

import dagger.Module;
import dagger.Provides;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import juanky.soriano.instagrambutsimpler.service.InstagramButSimplerDatabase;
import juanky.soriano.instagrambutsimpler.domain.IOPhoto;
import juanky.soriano.instagrambutsimpler.domain.Photo;
import juanky.soriano.instagrambutsimpler.domain.Converter;
import juanky.soriano.instagrambutsimpler.injection.PerActivity;
import juanky.soriano.instagrambutsimpler.support.RxExecutor;
import juanky.soriano.instagrambutsimpler.support.StateChangeService;
import juanky.soriano.instagrambutsimpler.support.Status;
import retrofit2.Retrofit;

@PerActivity
@Module
public class GalleryServiceModule {

    @Provides
    @PerActivity
    RxExecutor rxExecutor() {
        return RxExecutor.create(AndroidSchedulers.mainThread(), Schedulers.io());
    }

    @Provides
    @PerActivity
    GalleryAPI galleryAPI(Retrofit retrofit) {
        return retrofit.create(GalleryAPI.class);
    }

    @Provides
    @PerActivity
    GalleryDAO galleryDao(InstagramButSimplerDatabase database) {
        return database.galleryDao();
    }

    @Provides
    @PerActivity
    GalleryService galleryService(RxExecutor rxExecutor,
                                  GalleryUpdater updater,
                                  GalleryLoader loader) {
        return new GalleryService(rxExecutor, updater, loader);
    }

    @Provides
    @PerActivity
    GalleryLoader galleryLoader(GalleryDAO galleryDAO, Converter<IOPhoto, Photo> converter, StateChangeService<Status> stateChangeService) {
        return new AndroidGalleryLoader(galleryDAO, converter, stateChangeService);
    }

    @Provides
    @PerActivity
    GalleryUpdater galleryUpdater(GalleryAPI galleryAPI, GalleryDAO galleryDAO, StateChangeService<Status> stateChangeService) {
        return new AndroidGalleryUpdater(galleryAPI, galleryDAO, stateChangeService);
    }

    @Provides
    @PerActivity
    StateChangeService<Status> stateChangeService() {
        return new StateChangeService<>(PublishSubject.<Status>create());
    }
}
