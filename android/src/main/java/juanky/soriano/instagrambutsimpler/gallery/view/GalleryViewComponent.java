package juanky.soriano.instagrambutsimpler.gallery.view;

import dagger.Subcomponent;
import juanky.soriano.instagrambutsimpler.injection.PerActivity;
import juanky.soriano.instagrambutsimpler.view.PhotoView;

@PerActivity
@Subcomponent(modules = {GalleryViewModule.class})
public interface GalleryViewComponent {
    void inject(PhotoView view);
    void inject(GalleryView view);
}
