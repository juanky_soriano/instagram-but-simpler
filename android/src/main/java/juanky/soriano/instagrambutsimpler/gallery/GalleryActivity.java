package juanky.soriano.instagrambutsimpler.gallery;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;

import javax.inject.Inject;
import java.io.File;

import juanky.soriano.instagrambutsimpler.InstagramButSimplerActivity;
import juanky.soriano.instagrambutsimpler.navigation.Navigator;
import juanky.soriano.instagrambutsimpler.R;
import juanky.soriano.instagrambutsimpler.injection.Injector;
import juanky.soriano.instagrambutsimpler.support.DeviceForm;
import juanky.soriano.instagrambutsimpler.support.FileUtils;

public class GalleryActivity extends InstagramButSimplerActivity {
    private static final int TAKE_PHOTO_REQUEST_CODE = 1984;

    @Inject
    DeviceForm deviceForm;
    @Inject
    GalleryPresenter presenter;
    @Inject
    Navigator navigator;

    private File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        Injector.inject(this);

        setPreferredOrientation();
        findViewById(R.id.take_picture_button).setOnClickListener(onTakePictureClicked);
    }

    private void setPreferredOrientation() {
        if (deviceForm.isPhone()) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    private final View.OnClickListener onTakePictureClicked = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            file = FileUtils.createImageFile(getApplicationContext());
            navigator.toTakePhoto(TAKE_PHOTO_REQUEST_CODE, file);
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        presenter.startPresenting();
    }

    @Override
    protected void onStop() {
        presenter.stopPresenting();
        super.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TAKE_PHOTO_REQUEST_CODE && resultCode == RESULT_OK) {
            navigator.toNewPhoto(file);
        }
    }
}
