package juanky.soriano.instagrambutsimpler.gallery.view;

import android.app.Application;
import android.content.res.Resources;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;

import dagger.Module;
import dagger.Provides;
import juanky.soriano.instagrambutsimpler.R;
import juanky.soriano.instagrambutsimpler.injection.PerActivity;
import juanky.soriano.instagrambutsimpler.support.DeviceForm;

@PerActivity
@Module
public class GalleryViewModule {
    @Provides
    @PerActivity
    GalleryAdapter galleryAdapter() {
        return GalleryAdapter.newInstance();
    }

    @Provides
    @PerActivity
    RecyclerView.LayoutManager layoutManager(Application application, DeviceForm deviceForm, int bestSpanSize) {
        if (deviceForm.isTablet()) {
            return new GridLayoutManager(application, bestSpanSize);
        } else {
            return new LinearLayoutManager(application, LinearLayoutManager.VERTICAL, false);
        }
    }

    @Provides
    @PerActivity
    int bestSpanSize(Resources resources, DisplayMetrics displayMetrics) {
        return displayMetrics.widthPixels / resources.getDimensionPixelSize(R.dimen.photo_in_gallery_image_height);
    }

    @Provides
    @PerActivity
    LayoutInflater layoutInflater(Application application) {
        return LayoutInflater.from(application);
    }
}
