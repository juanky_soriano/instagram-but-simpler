package juanky.soriano.instagrambutsimpler.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import javax.inject.Inject;

import juanky.soriano.instagrambutsimpler.R;
import juanky.soriano.instagrambutsimpler.domain.Photo;
import juanky.soriano.instagrambutsimpler.injection.Injector;
import juanky.soriano.instagrambutsimpler.navigation.NotConvertibleException;
import juanky.soriano.instagrambutsimpler.navigation.Shareable;
import juanky.soriano.instagrambutsimpler.navigation.Transitable;
import juanky.soriano.instagrambutsimpler.support.DeviceForm;
import juanky.soriano.instagrambutsimpler.view.imageloader.ImageLoader;

public class PhotoView extends CardView implements Transitable, Shareable {
    private TextView titleTextView;
    private ImageView photoImageView;
    private TextView commentTextView;

    @Inject
    ImageLoader imageLoader;
    @Inject
    DeviceForm deviceForm;

    public PhotoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Injector.inject(this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        titleTextView = findViewById(R.id.photo_title);
        photoImageView = findViewById(R.id.photo_image);
        commentTextView = findViewById(R.id.photo_comment);
    }

    public void updateWith(Photo photo, PhotoActionListener listener) {
        updatePhoto(photo);
        updateListener(photo, listener);
    }

    private void updatePhoto(Photo photo) {
        updateTitle(photo);

        imageLoader.load(photo, photoImageView);

        updateComment(photo);
    }

    private void updateTitle(Photo photo) {
        String title = photo.title().rawTitle();
        setContentDescription(title);
        titleTextView.setText(title);
        int visibility = title.isEmpty() && deviceForm.isPhone() ? View.GONE : View.VISIBLE;
        titleTextView.setVisibility(visibility);
    }

    private void updateComment(Photo photo) {
        String comment = photo.comment().rawComment();
        commentTextView.setText(comment);
        int visibility = comment.isEmpty() && deviceForm.isPhone() ? View.GONE : View.VISIBLE;
        commentTextView.setVisibility(visibility);
    }

    private void updateListener(final Photo photo, final PhotoActionListener listener) {
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClick(photo, PhotoView.this);
            }
        });
    }

    @Override
    public String name() {
        return ViewCompat.getTransitionName(this);
    }

    @Override
    @SuppressWarnings("unchecked") // checked by instance comparison
    public <T> T transitAs(Class<T> type) throws NotConvertibleException {
        if (type != null && type.isInstance(this)) {
            return (T) this;
        }
        throw NotConvertibleException.notConvertible(type);
    }

    @Override
    @SuppressWarnings("unchecked") // checked by instance comparison
    public <T> T shareAs(Class<T> type) throws NotConvertibleException {
        if (type != null && type == Bitmap.class) {
            Bitmap bitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            layout(getLeft(), getTop(), getRight(), getBottom());
            draw(canvas);
            return (T) bitmap;
        }
        throw NotConvertibleException.notConvertible(type);
    }
}
