package juanky.soriano.instagrambutsimpler.view;

import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.view.View;

public class SnackbarDisplayer {
    public void show(View parentView, @StringRes int stringResId) {
        Snackbar.make(parentView, stringResId, Snackbar.LENGTH_LONG).show();
    }
}
