package juanky.soriano.instagrambutsimpler.view.imageloader;

import android.util.DisplayMetrics;
import android.webkit.URLUtil;
import android.widget.ImageView;

import com.bumptech.glide.DrawableRequestBuilder;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import javax.inject.Inject;
import java.net.URI;
import java.util.Locale;

import juanky.soriano.instagrambutsimpler.domain.Photo;

public class ImageLoader {
    private static final String IMAGE_TEMPLATE = "%s&%s";

    private final RequestManager requestManager;
    private final DisplayMetrics displayMetrics;

    @Inject
    public ImageLoader(RequestManager requestManager, DisplayMetrics displayMetrics) {
        this.requestManager = requestManager;
        this.displayMetrics = displayMetrics;
    }

    public void load(URI uri, ImageView imageView) {
        load(uri.toString(), imageView);
    }

    private DrawableRequestBuilder<String> load(String rawUri) {
        return requestManager.load(rawUri)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .override(displayMetrics.widthPixels, displayMetrics.heightPixels)
                .fitCenter()
                .dontAnimate();
    }

    public void load(Photo photo, ImageView imageView) {
        String rawPhotoUri = formatUriFor(photo);
        load(rawPhotoUri, imageView);
    }

    private String formatUriFor(Photo photo) {
        String rawUri = photo.image().imageUri().toString();
        return isFromNetwork(rawUri) ? String.format(Locale.US, IMAGE_TEMPLATE, rawUri, photo.id().rawId()) : rawUri;
    }

    private boolean isFromNetwork(String rawUri) {
        return URLUtil.isHttpsUrl(rawUri) || URLUtil.isHttpUrl(rawUri);
    }

    private void load(String rawPhotoUri, ImageView imageView) {
        load(rawPhotoUri).into(imageView);
    }

}
