package juanky.soriano.instagrambutsimpler.injection;

import juanky.soriano.instagrambutsimpler.DaggerInstagramButSimplerComponent;
import juanky.soriano.instagrambutsimpler.InstagramButSimplerApplication;
import juanky.soriano.instagrambutsimpler.InstagramButSimplerComponent;
import juanky.soriano.instagrambutsimpler.InstagramButSimplerModule;
import juanky.soriano.instagrambutsimpler.gallery.GalleryActivity;
import juanky.soriano.instagrambutsimpler.gallery.GalleryActivityModule;
import juanky.soriano.instagrambutsimpler.gallery.view.GalleryView;
import juanky.soriano.instagrambutsimpler.newphoto.NewPhotoActivity;
import juanky.soriano.instagrambutsimpler.newphoto.NewPhotoActivityModule;
import juanky.soriano.instagrambutsimpler.photodetails.PhotoDetailsActivity;
import juanky.soriano.instagrambutsimpler.photodetails.PhotoDetailsActivityModule;
import juanky.soriano.instagrambutsimpler.view.PhotoView;

public final class Injector {

    private static InstagramButSimplerComponent component;

    private Injector() {
        //non-instantiable
    }

    public static void init(InstagramButSimplerApplication application) {
        if (component == null) {
            component = DaggerInstagramButSimplerComponent.builder()
                    .instagramButSimplerModule(new InstagramButSimplerModule(application))
                    .build();
        }
    }

    public static void inject(GalleryActivity activity) {
        component.plusGalleryActivitySubcomponent(new GalleryActivityModule(activity)).inject(activity);
    }

    public static void inject(PhotoDetailsActivity activity) {
        component.plusPhotoDetailsActivitySubcomponent(new PhotoDetailsActivityModule(activity)).inject(activity);
    }

    public static void inject(NewPhotoActivity activity) {
        component.plusNewPhotoActivitySubcomponent(new NewPhotoActivityModule(activity)).inject(activity);
    }

    public static void inject(PhotoView photoView) {
        component.plusGalleryViewSubcomponent().inject(photoView);
    }

    public static void inject(GalleryView galleryView) {
        component.plusGalleryViewSubcomponent().inject(galleryView);
    }
}
