package juanky.soriano.instagrambutsimpler;

import dagger.Component;
import juanky.soriano.instagrambutsimpler.gallery.GalleryActivityComponent;
import juanky.soriano.instagrambutsimpler.gallery.GalleryActivityModule;
import juanky.soriano.instagrambutsimpler.gallery.view.GalleryViewComponent;
import juanky.soriano.instagrambutsimpler.injection.PerApplication;
import juanky.soriano.instagrambutsimpler.newphoto.NewPhotoActivityComponent;
import juanky.soriano.instagrambutsimpler.newphoto.NewPhotoActivityModule;
import juanky.soriano.instagrambutsimpler.photodetails.PhotoDetailsActivityComponent;
import juanky.soriano.instagrambutsimpler.photodetails.PhotoDetailsActivityModule;

@PerApplication
@Component(modules = {InstagramButSimplerModule.class})
public interface InstagramButSimplerComponent {
    GalleryActivityComponent plusGalleryActivitySubcomponent(GalleryActivityModule galleryActivityModule);

    GalleryViewComponent plusGalleryViewSubcomponent();

    PhotoDetailsActivityComponent plusPhotoDetailsActivitySubcomponent(PhotoDetailsActivityModule photoDetailsActivityModule);

    NewPhotoActivityComponent plusNewPhotoActivitySubcomponent(NewPhotoActivityModule newPhotoActivityModule);
}
