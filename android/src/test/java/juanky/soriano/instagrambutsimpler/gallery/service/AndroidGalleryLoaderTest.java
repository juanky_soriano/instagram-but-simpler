package juanky.soriano.instagrambutsimpler.gallery.service;

import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import io.reactivex.observers.TestObserver;
import io.reactivex.subjects.PublishSubject;
import juanky.soriano.instagrambutsimpler.domain.Converter;
import juanky.soriano.instagrambutsimpler.domain.Gallery;
import juanky.soriano.instagrambutsimpler.domain.IOPhoto;
import juanky.soriano.instagrambutsimpler.domain.Photo;
import juanky.soriano.instagrambutsimpler.support.StateChangeService;
import juanky.soriano.instagrambutsimpler.support.Status;

import static juanky.soriano.instagrambutsimpler.domain.IOPhotoFixture.anIOPhoto;
import static org.mockito.BDDMockito.given;

public class AndroidGalleryLoaderTest {
    private static final IOPhoto IO_PHOTO = anIOPhoto().build();
    private static final List<IOPhoto> IO_PHOTOS = Collections.singletonList(IO_PHOTO);
    private static final Photo PHOTO = anIOPhoto().buildAsPhoto();
    private static final List<Photo> PHOTOS = Collections.singletonList(PHOTO);
    private static final Gallery GALLERY = Gallery.create(PHOTOS);

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private GalleryDAO dao;
    @Mock
    private Converter<IOPhoto, Photo> converter;

    private StateChangeService<Status> stateChangeService = new StateChangeService<>(PublishSubject.<Status>create());
    private AndroidGalleryLoader loader;

    @Before
    public void setUp() {
        loader = new AndroidGalleryLoader(dao, converter, stateChangeService);
    }

    @Test
    public void givenErrorLoadingFromDAO_whenLoadingPhotos_thenReturnsException() {
        given(dao.load()).willThrow(Exception.class);

        TestObserver<Gallery> observer = new TestObserver<>();
        loader.loadGallery().subscribe(observer);

        observer.assertError(Exception.class);
    }

    @Test
    public void givenPhotosWillLoadFromDAO_whenLoadingPhoto_thenReturnsGallery() {
        given(dao.load()).willReturn(IO_PHOTOS);
        given(converter.from(IO_PHOTO)).willReturn(PHOTO);

        TestObserver<Gallery> observer = new TestObserver<>();
        loader.loadGallery().subscribe(observer);

        observer.assertValue(GALLERY);
    }

    @Test
    public void givenPhotosWillLoadFromDAO_whenStateChangesToError_thenReturnsGallery() {
        given(dao.load()).willReturn(IO_PHOTOS);
        given(converter.from(IO_PHOTO)).willReturn(PHOTO);

        TestObserver<Gallery> observer = new TestObserver<>();
        loader.loadGallery().subscribe(observer);
        stateChangeService.notify(Status.ERROR);

        observer.assertValues(GALLERY, GALLERY);
    }

    @Test
    public void givenPhotosWillLoadFromDAO_whenStateChangesToIdle_thenReturnsGallery() {
        given(dao.load()).willReturn(IO_PHOTOS);
        given(converter.from(IO_PHOTO)).willReturn(PHOTO);

        TestObserver<Gallery> observer = new TestObserver<>();
        loader.loadGallery().subscribe(observer);
        stateChangeService.notify(Status.IDLE);

        observer.assertValues(GALLERY, GALLERY);
    }

}
