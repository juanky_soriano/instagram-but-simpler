package juanky.soriano.instagrambutsimpler.gallery.service;

import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.stubbing.Answer;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import juanky.soriano.instagrambutsimpler.domain.IOPhoto;
import juanky.soriano.instagrambutsimpler.domain.IOPhotoFixture;
import juanky.soriano.instagrambutsimpler.support.StateChangeService;
import juanky.soriano.instagrambutsimpler.support.Status;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

public class AndroidGalleryUpdaterTest {

    private static final IOPhoto IO_PHOTO = IOPhotoFixture.anIOPhoto().build();
    private static final List<IOPhoto> IO_PHOTOS = Collections.singletonList(IO_PHOTO);
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private GalleryAPI api;
    @Mock
    private GalleryDAO dao;
    @Mock
    private StateChangeService<Status> stateChangeService;

    private TestObserver testObserver = new TestObserver();

    private AndroidGalleryUpdater updater;

    @Before
    public void setUp() {
        updater = new AndroidGalleryUpdater(api, dao, stateChangeService);
    }

    @Test
    public void whenUpdating_thenNotifiesLoading() {
        given(api.load()).willReturn(Observable.<List<IOPhoto>>empty());

        updater.update().subscribe(testObserver);

        verify(stateChangeService).notify(Status.LOADING);
    }

    @Test
    public void givenErrorLoadingFromAPI_whenUpdating_thenNotifiesError() {
        Exception exception = new Exception();
        given(api.load()).willReturn(Observable.<List<IOPhoto>>error(exception));

        updater.update().subscribe(testObserver);

        testObserver.assertError(exception);
    }

    @Test
    public void givenErrorStoringOnDatabase_whenUpdating_thenNotifiesError() {
        given(api.load()).willReturn(Observable.just(IO_PHOTOS));
        Exception exception = givenErrorStoringOnDatabase();

        updater.update().subscribe(testObserver);

        testObserver.assertError(exception);
    }

    @Test
    public void givenSuccessLoadingFromAPI_andStoringOnDatabase_whenUpdating_thenNotifiesIdle() {
        given(api.load()).willReturn(Observable.just(IO_PHOTOS));
        givenSuccessStoringOnDatabase();

        updater.update().subscribe(testObserver);

        verify(stateChangeService).notify(Status.IDLE);

    }

    @Test
    public void givenSuccessLoadingFromAPI_whenUpdating_thenStoresOnDatabase() {
        given(api.load()).willReturn(Observable.just(IO_PHOTOS));

        updater.update().subscribe(testObserver);

        verify(dao).insert(IO_PHOTOS);
    }

    private Exception givenErrorStoringOnDatabase() {
        final Exception exception = new Exception();
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                throw exception;
            }
        }).when(dao).insert(IO_PHOTOS);
        return exception;
    }

    private void givenSuccessStoringOnDatabase() {
        doNothing().when(dao).insert(IO_PHOTOS);
    }

}
