package juanky.soriano.instagrambutsimpler.photodetails.service;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import io.reactivex.observers.TestObserver;
import juanky.soriano.instagrambutsimpler.domain.Converter;
import juanky.soriano.instagrambutsimpler.domain.IOPhoto;
import juanky.soriano.instagrambutsimpler.domain.Photo;
import juanky.soriano.instagrambutsimpler.domain.PhotoId;

import static juanky.soriano.instagrambutsimpler.domain.IOPhotoFixture.anIOPhoto;
import static org.mockito.BDDMockito.given;

public class AndroidPhotoLoaderTest {

    private static final Long RAW_ID = 10L;
    private static final PhotoId PHOTO_ID = PhotoId.create(RAW_ID);
    private static final IOPhoto IO_PHOTO = anIOPhoto().build();
    private static final Photo PHOTO = anIOPhoto().buildAsPhoto();

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private PhotoDAO dao;
    @Mock
    private Converter<IOPhoto, Photo> converter;

    private AndroidPhotoLoader loader;

    @Before
    public void setUp() {
        loader = new AndroidPhotoLoader(dao, converter);
    }

    @Test
    public void givenErrorLoadingFromDAO_whenLoadingPhoto_thenReturnsException() {
        given(dao.retrieveById(RAW_ID)).willThrow(Exception.class);

        TestObserver<Photo> observer = new TestObserver<>();
        loader.loadPhoto(PHOTO_ID).subscribe(observer);

        observer.assertError(Exception.class);
    }

    @Test
    public void givenPhotoWillLoadFromDAO_whenLoadingPhoto_thenReturnsPhoto() {
        given(dao.retrieveById(RAW_ID)).willReturn(IO_PHOTO);
        given(converter.from(IO_PHOTO)).willReturn(PHOTO);

        TestObserver<Photo> observer = new TestObserver<>();
        loader.loadPhoto(PHOTO_ID).subscribe(observer);

        observer.assertValue(PHOTO);
    }
}
