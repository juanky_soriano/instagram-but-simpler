package juanky.soriano.instagrambutsimpler.support;

import android.content.res.Resources;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import juanky.soriano.instagrambutsimpler.R;

import static juanky.soriano.instagrambutsimpler.R.bool.isTablet;
import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

public class DeviceFormTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private Resources resources;

    private DeviceForm deviceForm;

    @Before
    public void setUp() {
        deviceForm = new DeviceForm(resources);
    }

    @Test
    public void givenDeviceIsTablet_whenGettingIfDeviceIsTablet_thenReturnsTrue() {
        given(resources.getBoolean(isTablet)).willReturn(true);

        boolean isTablet = deviceForm.isTablet();

        assertThat(isTablet).isTrue();
    }

    @Test
    public void givenDeviceIsPhone_whenGettingIfDeviceIsTablet_thenReturnsFalse() {
        given(resources.getBoolean(isTablet)).willReturn(false);

        boolean isTablet = deviceForm.isTablet();

        assertThat(isTablet).isFalse();
    }

    @Test
    public void givenDeviceIsTablet_whenGettingIfDeviceIsPhone_thenReturnsFalse() {
        given(resources.getBoolean(isTablet)).willReturn(true);

        boolean isPhone = deviceForm.isPhone();

        assertThat(isPhone).isFalse();
    }

    @Test
    public void givenDeviceIsPhone_whenGettingIfDeviceIsPhone_thenReturnsTrue() {
        given(resources.getBoolean(R.bool.isTablet)).willReturn(false);

        boolean isPhone = deviceForm.isPhone();

        assertThat(isPhone).isTrue();
    }

}
