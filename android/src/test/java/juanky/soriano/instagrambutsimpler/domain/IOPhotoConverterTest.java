package juanky.soriano.instagrambutsimpler.domain;

import org.junit.Before;
import org.junit.Test;

import static juanky.soriano.instagrambutsimpler.domain.PhotoFixture.aPhoto;
import static org.fest.assertions.api.Assertions.assertThat;

public class IOPhotoConverterTest {

    private IOPhotoConverter converter;

    @Before
    public void setUp() {
        converter = new IOPhotoConverter();
    }

    @Test
    public void givenPhoto_whenConvertingPhoto_thenReturnsExpectedIOPhoto() {
        Photo givenPhoto = aPhoto().build();

        IOPhoto ioPhoto = converter.from(givenPhoto);

        assertThat(ioPhoto).isEqualTo(expectedIoPhoto(givenPhoto));
    }

    private IOPhoto expectedIoPhoto(Photo photo) {
        IOPhoto ioPhoto = new IOPhoto();
        ioPhoto.setId(photo.id().rawId());
        ioPhoto.setTitle(photo.title().rawTitle());
        ioPhoto.setPhotoUri(photo.image().imageUri().toString());
        ioPhoto.setComment(photo.comment().rawComment());
        ioPhoto.setPublishedAt(photo.date());

        return ioPhoto;
    }

}
