package juanky.soriano.instagrambutsimpler.domain;

import java.net.URI;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import static org.fest.assertions.api.Assertions.assertThat;

public class PhotoConverterTest {

    private PhotoConverter converter;

    @Before
    public void setUp() {
        converter = new PhotoConverter();
    }

    @Test
    public void givenIOPhoto_whenConverting_thenReturnsExpectedPhoto() {
        IOPhoto ioPhoto = givenIOPhoto();

        Photo photo = converter.from(ioPhoto);

        assertThat(photo).isEqualTo(expectedPhoto(ioPhoto));
    }

    private IOPhoto givenIOPhoto() {
        IOPhoto ioPhoto = new IOPhoto();
        ioPhoto.setId(0L);
        ioPhoto.setTitle("title");
        ioPhoto.setPhotoUri("http://www.uri.com");
        ioPhoto.setComment("comment");
        ioPhoto.setPublishedAt(new Date());

        return ioPhoto;
    }

    private Photo expectedPhoto(IOPhoto ioPhoto) {
        PhotoId id = PhotoId.create(ioPhoto.id());
        Title title = Title.create(ioPhoto.title());
        Image image = Image.create(URI.create(ioPhoto.photoUri()));
        Comment comment = Comment.create(ioPhoto.comment());
        Date date = ioPhoto.publishedAt();
        return Photo.create(id, title, image, comment, date);
    }

}
