package juanky.soriano.instagrambutsimpler.domain;

import java.util.Date;

public class IOPhotoFixture {

    private long id = 0L;
    private String title = "title";
    private String photoUri = "http://www.image.com";
    private String comment = "comment";
    private Date publishedAt = new Date();

    private IOPhotoFixture() {
        //non-instantiable
    }

    public static IOPhotoFixture anIOPhoto() {
        return new IOPhotoFixture();
    }

    public static IOPhotoFixture from(Photo photo) {
        IOPhotoFixture fixture = anIOPhoto();
        fixture.id = photo.id().rawId();
        fixture.title = photo.title().rawTitle();
        fixture.photoUri = photo.image().imageUri().toString();
        fixture.comment = photo.comment().rawComment();
        fixture.publishedAt = new Date(photo.date().getTime());

        return fixture;
    }

    public IOPhoto build() {
        IOPhoto ioPhoto = new IOPhoto();
        ioPhoto.setId(id);
        ioPhoto.setTitle(title);
        ioPhoto.setPhotoUri(photoUri);
        ioPhoto.setComment(comment);
        ioPhoto.setPublishedAt(publishedAt);
        return ioPhoto;
    }

    public Photo buildAsPhoto() {
        return new PhotoConverter().from(build());
    }
}
