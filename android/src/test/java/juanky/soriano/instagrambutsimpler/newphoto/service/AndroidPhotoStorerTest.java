package juanky.soriano.instagrambutsimpler.newphoto.service;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.stubbing.Answer;

import io.reactivex.observers.TestObserver;
import juanky.soriano.instagrambutsimpler.domain.Converter;
import juanky.soriano.instagrambutsimpler.domain.IOPhoto;
import juanky.soriano.instagrambutsimpler.domain.Photo;

import static juanky.soriano.instagrambutsimpler.domain.IOPhotoFixture.anIOPhoto;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;

public class AndroidPhotoStorerTest {
    private static final IOPhoto IO_PHOTO = anIOPhoto().build();
    private static final Photo PHOTO = anIOPhoto().buildAsPhoto();

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private NewPhotoDAO dao;
    @Mock
    private Converter<Photo, IOPhoto> converter;

    private AndroidPhotoStorer loader;

    @Before
    public void setUp() {
        loader = new AndroidPhotoStorer(dao, converter);
    }

    @Test
    public void givenErrorStoringInDAO_whenStoringPhoto_thenReturnsException() {
        givenErrorStoringInDAO();

        TestObserver<Photo> observer = new TestObserver<>();
        loader.store(PHOTO).subscribe(observer);

        observer.assertError(Exception.class);
    }

    @Test
    public void givenPhoto_whenStoringPhotos_thenCompletes() {
        given(converter.from(PHOTO)).willReturn(IO_PHOTO);

        TestObserver observer = new TestObserver<>();
        loader.store(PHOTO).subscribe(observer);

        observer.assertComplete();
    }

    @Test
    public void givenPhoto_whenStoringPhoto_thenStoresPhotoInDAO() {
        given(converter.from(PHOTO)).willReturn(IO_PHOTO);

        TestObserver observer = new TestObserver<>();
        loader.store(PHOTO).subscribe(observer);

        verify(dao).insert(IO_PHOTO);
    }

    private void givenErrorStoringInDAO() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                throw new Exception();
            }
        }).when(dao).insert(IO_PHOTO);
    }

}
