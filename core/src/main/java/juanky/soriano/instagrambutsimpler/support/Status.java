package juanky.soriano.instagrambutsimpler.support;

public enum Status {
    ERROR,
    IDLE,
    LOADING
}
