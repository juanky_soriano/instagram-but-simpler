package juanky.soriano.instagrambutsimpler.support;

public class SimpleCompletableObserver {

    public void onSubscribe() {
        //to be overridden if wanted
    }

    public void onComplete() {
        //to be overridden if wanted
    }

    public void onError(Throwable e) {
        //to be overridden if wanted
    }
}
