package juanky.soriano.instagrambutsimpler.support;

import java.util.Arrays;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Predicate;
import io.reactivex.subjects.Subject;

public class StateChangeService<T> {

    private final Subject<T> publishSubject;

    public StateChangeService(Subject<T> publishSubject) {
        this.publishSubject = publishSubject;
    }

    public Observable<T> observeStateChanges(T... state) {
        return publishSubject
                .filter(onlyStateChangesIn(Arrays.asList(state)));
    }

    private Predicate<T> onlyStateChangesIn(final List<T> stateChanges) {
        return new Predicate<T>() {
            @Override
            public boolean test(T emittedStateChange) {
                return stateChanges.contains(emittedStateChange);
            }
        };
    }

    public void notify(T state) {
        publishSubject.onNext(state);
    }
}
