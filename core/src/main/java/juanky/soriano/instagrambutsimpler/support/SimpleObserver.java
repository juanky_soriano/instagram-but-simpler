package juanky.soriano.instagrambutsimpler.support;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

public class SimpleObserver<T> implements Observer<T> {
    @Override
    public void onSubscribe(@NonNull Disposable d) {
        // implement if required
    }

    @Override
    public void onNext(@NonNull T t) {
        // implement if required
    }

    @Override
    public void onError(@NonNull Throwable e) {
        // implement if require

    }

    @Override
    public void onComplete() {
        // implement if require

    }
}
