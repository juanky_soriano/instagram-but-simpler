package juanky.soriano.instagrambutsimpler.support;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableObserver;

public class RxExecutor {
    private final CompositeDisposable compositeDisposable;
    private final Scheduler observeScheduler;
    private final Scheduler subscribeScheduler;

    public static RxExecutor create(Scheduler observeScheduler, Scheduler subscribeScheduler) {
        return new RxExecutor(new CompositeDisposable(), observeScheduler, subscribeScheduler);
    }

    private RxExecutor(CompositeDisposable compositeDisposable, Scheduler observeScheduler, Scheduler subscribeScheduler) {
        this.compositeDisposable = compositeDisposable;
        this.subscribeScheduler = subscribeScheduler;
        this.observeScheduler = observeScheduler;
    }

    public <T> void observe(Observable<T> observable, final Observer<T> observer) {
        observe(observable, new DisposableObserver<T>() {
            @Override
            public void onNext(@NonNull T t) {
                observer.onNext(t);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                observer.onError(e);
            }

            @Override
            public void onComplete() {
                observer.onComplete();
            }
        });
    }

    private <T> void observe(Observable<T> observable, final DisposableObserver<T> observer) {
        Disposable disposable = observable.observeOn(observeScheduler)
                .subscribeOn(subscribeScheduler)
                .subscribeWith(observer);

        compositeDisposable.add(disposable);
    }

    public void observe(Completable completable, SimpleCompletableObserver observer) {
        Disposable disposable = completable.observeOn(observeScheduler)
                .subscribeOn(subscribeScheduler)
                .doOnSubscribe(forwardOnSubscribe(observer))
                .subscribe(forwardOnComplete(observer), forwardOnError(observer));

        compositeDisposable.add(disposable);
    }

    private Consumer<Disposable> forwardOnSubscribe(final SimpleCompletableObserver observer) {
        return new Consumer<Disposable>() {
            @Override
            public void accept(Disposable disposable) {
                observer.onSubscribe();
            }
        };
    }

    private Action forwardOnComplete(final SimpleCompletableObserver observer) {
        return new Action() {
            @Override
            public void run() {
                observer.onComplete();
            }
        };
    }

    private Consumer<Throwable> forwardOnError(final SimpleCompletableObserver observer) {
        return new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) {
                observer.onError(throwable);
            }
        };
    }

    public void stopObserving() {
        compositeDisposable.clear();
    }
}
