package juanky.soriano.instagrambutsimpler.domain;

import com.google.auto.value.AutoValue;

import java.util.Date;

@AutoValue
public abstract class Photo {

    public static Photo create(PhotoId id, Title title, Image image, Comment comment, Date date) {
        return new AutoValue_Photo(id, title, image, comment, date);
    }

    public abstract PhotoId id();

    boolean hasId() {
        return id().isRequired();
    }

    public abstract Title title();

    public abstract Image image();

    public abstract Comment comment();

    public abstract Date date();

    // Customising equals and hashcode to ignore date as everytime
    // the service is called date changes.
    // This shouldn't happen on a real API
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Photo otherPhoto = (Photo) o;

        return id().equals(otherPhoto.id())
                && title().equals(otherPhoto.title())
                && image().equals(otherPhoto.image())
                && comment().equals(otherPhoto.comment());

    }

    @Override
    public int hashCode() {
        int result = id().hashCode();
        result = 31 * result + title().hashCode();
        result = 31 * result + image().hashCode();
        result = 31 * result + comment().hashCode();
        return result;
    }
}
