package juanky.soriano.instagrambutsimpler.domain;

public interface Converter<From, To> {
    To from(From object);
}
