package juanky.soriano.instagrambutsimpler.domain;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class Comment {
    public static Comment create(String rawComment) {
        return new AutoValue_Comment(rawComment);
    }

    public abstract String rawComment();
}
