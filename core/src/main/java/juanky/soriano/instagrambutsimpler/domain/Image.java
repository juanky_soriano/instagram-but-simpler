package juanky.soriano.instagrambutsimpler.domain;

import com.google.auto.value.AutoValue;

import java.net.URI;

@AutoValue
public abstract class Image {
    public static Image create(URI imageUri) {
        return new AutoValue_Image(imageUri);
    }

    public abstract URI imageUri();
}
