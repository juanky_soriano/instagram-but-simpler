package juanky.soriano.instagrambutsimpler.domain;

import com.google.auto.value.AutoValue;

import java.util.ArrayList;
import java.util.List;

@AutoValue
public abstract class Gallery {
    public static Gallery create(List<Photo> photos) {
        List<Photo> photoList = new ArrayList<>(photos);
        return new AutoValue_Gallery(photoList);
    }

    public abstract List<Photo> photos();
}
