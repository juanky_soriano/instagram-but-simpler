package juanky.soriano.instagrambutsimpler.domain;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class Title {
    public static Title create(String rawTitle) {
        return new AutoValue_Title(rawTitle);
    }

    public abstract String rawTitle();
}
