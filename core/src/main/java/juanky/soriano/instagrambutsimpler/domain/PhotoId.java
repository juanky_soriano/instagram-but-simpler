package juanky.soriano.instagrambutsimpler.domain;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class PhotoId {
    public static PhotoId NOT_REQUIRED = PhotoId.create(-1);

    public static PhotoId create(long rawId) {
        return new AutoValue_PhotoId(rawId);
    }

    public abstract long rawId();

    boolean isRequired() {
        return this != NOT_REQUIRED;
    }
}
