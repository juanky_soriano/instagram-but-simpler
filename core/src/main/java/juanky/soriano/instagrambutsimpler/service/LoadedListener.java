package juanky.soriano.instagrambutsimpler.service;

public interface LoadedListener<T> {
    void onLoaded(T data);
}
