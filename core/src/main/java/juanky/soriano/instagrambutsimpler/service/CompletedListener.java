package juanky.soriano.instagrambutsimpler.service;

public interface CompletedListener {
    void onCompleted();
}
