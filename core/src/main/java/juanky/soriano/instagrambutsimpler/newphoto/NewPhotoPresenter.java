package juanky.soriano.instagrambutsimpler.newphoto;

import javax.inject.Inject;
import java.net.URI;

import juanky.soriano.instagrambutsimpler.domain.Photo;
import juanky.soriano.instagrambutsimpler.newphoto.service.NewPhotoService;
import juanky.soriano.instagrambutsimpler.service.CompletedListener;
import juanky.soriano.instagrambutsimpler.support.SimpleCompletableObserver;

class NewPhotoPresenter {
    private final NewPhotoDisplayer displayer;
    private final NewPhotoService service;

    private CompletedListener completedListener;

    @Inject
    NewPhotoPresenter(NewPhotoDisplayer displayer, NewPhotoService service) {
        this.displayer = displayer;
        this.service = service;
    }

    void startPresenting(URI photoUri, CompletedListener completedListener) {
        displayer.attach(actionListener);
        displayer.displayFor(photoUri);
        this.completedListener = completedListener;
    }

    private final NewPhotoDisplayer.ActionListener actionListener = new NewPhotoDisplayer.ActionListener() {
        @Override
        public void onNewPhoto(Photo photo) {
            service.storePhoto(photo, observer);
        }
    };

    private final SimpleCompletableObserver observer = new SimpleCompletableObserver() {
        @Override
        public void onComplete() {
            completedListener.onCompleted();
        }

        @Override
        public void onError(Throwable e) {
            displayer.displayErrorStoring();
        }
    };

    void stopPresenting() {
        displayer.detach();
        service.close();
        completedListener = null;
    }
}
