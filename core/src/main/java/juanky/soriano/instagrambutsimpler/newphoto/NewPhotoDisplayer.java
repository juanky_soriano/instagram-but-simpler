package juanky.soriano.instagrambutsimpler.newphoto;

import java.net.URI;

import juanky.soriano.instagrambutsimpler.domain.Photo;

interface NewPhotoDisplayer {

    void attach(ActionListener listener);

    void displayFor(URI photoUri);

    void displayErrorStoring();

    void detach();

    interface ActionListener {
        void onNewPhoto(Photo photo);

        ActionListener NO_OP = new ActionListener() {
            @Override
            public void onNewPhoto(Photo photo) {
                //no-op
            }
        };

    }
}
