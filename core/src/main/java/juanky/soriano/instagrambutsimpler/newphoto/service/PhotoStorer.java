package juanky.soriano.instagrambutsimpler.newphoto.service;

import io.reactivex.Completable;
import juanky.soriano.instagrambutsimpler.domain.Photo;

interface PhotoStorer {
    Completable store(Photo photo);
}
