package juanky.soriano.instagrambutsimpler.newphoto.service;

import javax.inject.Inject;
import java.io.Closeable;

import juanky.soriano.instagrambutsimpler.domain.Photo;
import juanky.soriano.instagrambutsimpler.support.RxExecutor;
import juanky.soriano.instagrambutsimpler.support.SimpleCompletableObserver;

public class NewPhotoService implements Closeable {
    private final RxExecutor rxExecutor;
    private final PhotoStorer storer;

    @Inject
    NewPhotoService(RxExecutor rxExecutor,
                    PhotoStorer storer) {
        this.rxExecutor = rxExecutor;
        this.storer = storer;
    }

    public void storePhoto(Photo photo, SimpleCompletableObserver observer) {
        rxExecutor.observe(storer.store(photo), observer);
    }

    @Override
    public void close() {
        rxExecutor.stopObserving();
    }
}
