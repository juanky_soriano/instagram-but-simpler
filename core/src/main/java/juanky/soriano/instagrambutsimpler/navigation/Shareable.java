package juanky.soriano.instagrambutsimpler.navigation;

public interface Shareable {
    <T> T shareAs(Class<T> clazz) throws NotConvertibleException;
}
