package juanky.soriano.instagrambutsimpler.navigation;

public class NotConvertibleException extends IllegalStateException {
    public static NotConvertibleException notConvertible(Class clazz) {
        return new NotConvertibleException(clazz);
    }

    private NotConvertibleException(Class clazz) {
        super("Cannot convert to the given class: " + clazz.getCanonicalName());
    }
}
