package juanky.soriano.instagrambutsimpler.navigation;

public interface Transitable {
    String name();

    <T> T transitAs(Class<T> type) throws NotConvertibleException;

}
