package juanky.soriano.instagrambutsimpler.navigation;

import java.io.File;

import juanky.soriano.instagrambutsimpler.domain.Photo;

public interface Navigator {
    void toDetailsOf(Photo photo, Transitable transitable);

    void toTakePhoto(int requestCode, File photoFile);

    void toNewPhoto(File file);

    void toSharePhoto(Shareable shareable);
}
