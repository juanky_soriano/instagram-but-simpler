package juanky.soriano.instagrambutsimpler.gallery.service;

import io.reactivex.Observable;
import juanky.soriano.instagrambutsimpler.domain.Gallery;

interface GalleryLoader {

    Observable<Gallery> loadGallery();
}
