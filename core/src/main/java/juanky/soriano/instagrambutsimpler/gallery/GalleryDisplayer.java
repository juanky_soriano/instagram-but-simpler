package juanky.soriano.instagrambutsimpler.gallery;

import juanky.soriano.instagrambutsimpler.navigation.Transitable;
import juanky.soriano.instagrambutsimpler.domain.Gallery;
import juanky.soriano.instagrambutsimpler.domain.Photo;
import juanky.soriano.instagrambutsimpler.view.PhotoActionListener;

public interface GalleryDisplayer {
    void attach(ActionListener listener);

    void displayLoading();

    void display(Gallery gallery);

    void displayLoaded();

    void displayErrorUpdating();

    void displayErrorLoading();

    void detach();

    interface ActionListener extends PhotoActionListener {
        @Override
        void onClick(Photo photo, Transitable transitable);

        void onForceRefresh();

        ActionListener NO_OP = new ActionListener() {
            @Override
            public void onClick(Photo photo, Transitable transitable) {
                //no op
            }

            @Override
            public void onForceRefresh() {
                //no op
            }
        };
    }
}
