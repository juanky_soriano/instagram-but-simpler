package juanky.soriano.instagrambutsimpler.gallery;

import javax.inject.Inject;

import juanky.soriano.instagrambutsimpler.navigation.Navigator;
import juanky.soriano.instagrambutsimpler.navigation.Transitable;
import juanky.soriano.instagrambutsimpler.domain.Gallery;
import juanky.soriano.instagrambutsimpler.domain.Photo;
import juanky.soriano.instagrambutsimpler.gallery.service.GalleryService;
import juanky.soriano.instagrambutsimpler.support.SimpleCompletableObserver;
import juanky.soriano.instagrambutsimpler.support.SimpleObserver;

class GalleryPresenter {
    private final GalleryDisplayer displayer;
    private final GalleryService service;
    private final Navigator navigator;

    @Inject
    GalleryPresenter(GalleryDisplayer displayer, GalleryService service, Navigator navigator) {
        this.displayer = displayer;
        this.service = service;
        this.navigator = navigator;
    }

    void startPresenting() {
        displayer.attach(listener);
        service.updateGallery(updateObserver);
        service.loadGallery(loadObserver);
    }

    private final SimpleCompletableObserver updateObserver = new SimpleCompletableObserver() {
        @Override
        public void onSubscribe() {
            displayer.displayLoading();
        }

        @Override
        public void onComplete() {
            displayer.displayLoaded();
        }

        @Override
        public void onError(Throwable e) {
            displayer.displayErrorUpdating();
        }
    };

    private final SimpleObserver<Gallery> loadObserver = new SimpleObserver<Gallery>() {
        @Override
        public void onNext(Gallery gallery) {
            displayer.display(gallery);
        }

        @Override
        public void onError(Throwable e) {
            displayer.displayErrorLoading();
        }
    };

    private final GalleryDisplayer.ActionListener listener = new GalleryDisplayer.ActionListener() {
        @Override
        public void onClick(Photo photo, Transitable transitable) {
            navigator.toDetailsOf(photo, transitable);
        }

        @Override
        public void onForceRefresh() {
            service.updateGallery(updateObserver);
        }
    };

    void stopPresenting() {
        displayer.detach();
        service.close();
    }
}
