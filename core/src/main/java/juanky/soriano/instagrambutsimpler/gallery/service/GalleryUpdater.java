package juanky.soriano.instagrambutsimpler.gallery.service;

import io.reactivex.Completable;

interface GalleryUpdater {

    Completable update();
}
