package juanky.soriano.instagrambutsimpler.gallery.service;

import javax.inject.Inject;
import java.io.Closeable;

import juanky.soriano.instagrambutsimpler.domain.Gallery;
import juanky.soriano.instagrambutsimpler.support.RxExecutor;
import juanky.soriano.instagrambutsimpler.support.SimpleCompletableObserver;
import juanky.soriano.instagrambutsimpler.support.SimpleObserver;

public class GalleryService implements Closeable {
    private final RxExecutor rxExecutor;
    private final GalleryUpdater updater;
    private final GalleryLoader loader;

    @Inject
    public GalleryService(RxExecutor rxExecutor,
                          GalleryUpdater updater,
                          GalleryLoader loader) {
        this.rxExecutor = rxExecutor;
        this.updater = updater;
        this.loader = loader;
    }

    public void updateGallery(SimpleCompletableObserver observer) {
        rxExecutor.observe(updater.update(), observer);
    }

    public void loadGallery(SimpleObserver<Gallery> observer) {
        rxExecutor.observe(loader.loadGallery(), observer);
    }

    @Override
    public void close() {
        rxExecutor.stopObserving();
    }
}
