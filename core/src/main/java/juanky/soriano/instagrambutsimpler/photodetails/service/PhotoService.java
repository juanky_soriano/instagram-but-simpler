package juanky.soriano.instagrambutsimpler.photodetails.service;

import javax.inject.Inject;
import java.io.Closeable;

import juanky.soriano.instagrambutsimpler.domain.Photo;
import juanky.soriano.instagrambutsimpler.domain.PhotoId;
import juanky.soriano.instagrambutsimpler.support.RxExecutor;
import juanky.soriano.instagrambutsimpler.support.SimpleObserver;

public class PhotoService implements Closeable {
    private final RxExecutor rxExecutor;
    private final PhotoLoader loader;

    @Inject
    PhotoService(RxExecutor rxExecutor,
                 PhotoLoader loader) {
        this.rxExecutor = rxExecutor;
        this.loader = loader;
    }

    public void loadPhoto(PhotoId id, SimpleObserver<Photo> observer) {
        rxExecutor.observe(loader.loadPhoto(id), observer);
    }

    @Override
    public void close() {
        rxExecutor.stopObserving();
    }
}
