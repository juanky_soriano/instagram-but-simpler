package juanky.soriano.instagrambutsimpler.photodetails.service;

import io.reactivex.Observable;
import juanky.soriano.instagrambutsimpler.domain.Photo;
import juanky.soriano.instagrambutsimpler.domain.PhotoId;

interface PhotoLoader {

    Observable<Photo> loadPhoto(PhotoId id);
}
