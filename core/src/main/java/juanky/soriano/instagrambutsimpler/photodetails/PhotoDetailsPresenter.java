package juanky.soriano.instagrambutsimpler.photodetails;

import javax.inject.Inject;

import juanky.soriano.instagrambutsimpler.domain.Photo;
import juanky.soriano.instagrambutsimpler.domain.PhotoId;
import juanky.soriano.instagrambutsimpler.photodetails.service.PhotoService;
import juanky.soriano.instagrambutsimpler.service.LoadedListener;
import juanky.soriano.instagrambutsimpler.support.SimpleObserver;

class PhotoDetailsPresenter {
    private final PhotoDetailsDisplayer displayer;
    private final PhotoService service;

    private LoadedListener<Photo> listener;

    @Inject
    PhotoDetailsPresenter(PhotoDetailsDisplayer displayer, PhotoService service) {
        this.displayer = displayer;
        this.service = service;
    }

    void startPresenting(PhotoId photoId, LoadedListener<Photo> listener) {
        this.listener = listener;
        service.loadPhoto(photoId, loadObserver);
    }

    private final SimpleObserver<Photo> loadObserver = new SimpleObserver<Photo>() {

        @Override
        public void onNext(Photo photo) {
            listener.onLoaded(photo);
            displayer.display(photo);
        }

        @Override
        public void onError(Throwable e) {
            displayer.displayErrorLoading();
        }
    };

    void stopPresenting() {
        service.close();
    }
}
