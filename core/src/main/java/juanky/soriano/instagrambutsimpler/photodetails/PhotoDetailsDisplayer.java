package juanky.soriano.instagrambutsimpler.photodetails;

import java.io.Serializable;

import juanky.soriano.instagrambutsimpler.domain.Photo;

interface PhotoDetailsDisplayer {
    void display(Photo photo);

    void displayErrorLoading();

    interface ActionListener {
        void onShare(Serializable shareable);

        ActionListener NO_OP = new ActionListener() {
            @Override
            public void onShare(Serializable shareable) {

            }
        };

    }
}
