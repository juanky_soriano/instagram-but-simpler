package juanky.soriano.instagrambutsimpler.view;

import juanky.soriano.instagrambutsimpler.navigation.Transitable;
import juanky.soriano.instagrambutsimpler.domain.Photo;

public interface PhotoActionListener {
    void onClick(Photo photo, Transitable transitable);

    PhotoActionListener NO_OP = new PhotoActionListener() {
        @Override
        public void onClick(Photo photo, Transitable transitable) {
            //no-op
        }
    };
}
