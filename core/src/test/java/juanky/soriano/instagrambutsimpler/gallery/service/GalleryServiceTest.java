package juanky.soriano.instagrambutsimpler.gallery.service;

import java.util.Collections;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import juanky.soriano.instagrambutsimpler.domain.Gallery;
import juanky.soriano.instagrambutsimpler.domain.Photo;
import juanky.soriano.instagrambutsimpler.support.RxExecutor;
import juanky.soriano.instagrambutsimpler.support.SimpleCompletableObserver;
import juanky.soriano.instagrambutsimpler.support.SimpleObserver;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class GalleryServiceTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private GalleryUpdater updater;
    @Mock
    private GalleryLoader loader;
    @Mock
    private SimpleObserver<Gallery> loadObserver;
    @Mock
    private SimpleCompletableObserver updateObserver;

    private RxExecutor executor = RxExecutor.create(Schedulers.trampoline(), Schedulers.trampoline());

    private GalleryService service;

    @Before
    public void setUp() {
        service = new GalleryService(executor, updater, loader);
    }

    @Test
    public void givenErrorUpdatingGallery_whenUpdatingGallery_thenInvokesOnError() {
        Throwable throwable = new Throwable();
        given(updater.update()).willReturn(Completable.error(throwable));

        service.updateGallery(updateObserver);

        verify(updateObserver).onError(throwable);
    }

    @Test
    public void givenSuccessUpdatingGallery_whenUpdatingGallery_thenInvokesOnComplete() {
        given(updater.update()).willReturn(Completable.complete());

        service.updateGallery(updateObserver);

        verify(updateObserver).onComplete();
    }

    @Test
    public void givenErrorLoadingGallery_whenLoadingGallery_thenInvokesOnError() {
        Throwable throwable = new Throwable();
        given(loader.loadGallery()).willReturn(Observable.<Gallery>error(throwable));

        service.loadGallery(loadObserver);

        verify(loadObserver).onError(throwable);
    }

    @Test
    public void givenGalleryWillLoadSuccessfuly_whenLoadingGallery_thenInvokesOnNextForGalleryAndOnComplete() {
        Gallery gallery = Gallery.create(Collections.<Photo>emptyList());
        given(loader.loadGallery()).willReturn(Observable.just(gallery));

        service.loadGallery(loadObserver);

        InOrder inOrder = inOrder(loadObserver);
        inOrder.verify(loadObserver).onNext(gallery);
        inOrder.verify(loadObserver).onComplete();
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void whenClosing_thenStopsObserving() {
        RxExecutor rxExecutor = mock(RxExecutor.class);
        service = new GalleryService(rxExecutor, updater, loader);
        service.close();

        verify(rxExecutor).stopObserving();
    }
}
