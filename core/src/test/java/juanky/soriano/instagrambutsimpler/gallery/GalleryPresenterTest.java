package juanky.soriano.instagrambutsimpler.gallery;

import java.util.Collections;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import juanky.soriano.instagrambutsimpler.navigation.Navigator;
import juanky.soriano.instagrambutsimpler.navigation.Transitable;
import juanky.soriano.instagrambutsimpler.domain.Gallery;
import juanky.soriano.instagrambutsimpler.domain.Photo;
import juanky.soriano.instagrambutsimpler.gallery.service.GalleryService;
import juanky.soriano.instagrambutsimpler.support.SimpleCompletableObserver;
import juanky.soriano.instagrambutsimpler.support.SimpleObserver;

import static juanky.soriano.instagrambutsimpler.domain.PhotoFixture.aPhoto;
import static org.mockito.Mockito.*;

public class GalleryPresenterTest {
    private static final Photo PHOTO = aPhoto().build();
    private static final Transitable TRANSITABLE = mock(Transitable.class);

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private GalleryDisplayer displayer;
    @Mock
    private GalleryService service;
    @Mock
    private Navigator navigator;
    private GalleryPresenter presenter;

    private SimpleObserver<Gallery> loadObserver;
    private SimpleCompletableObserver updateObserver;
    private GalleryDisplayer.ActionListener listener;

    @Before
    public void setUp() {
        presenter = new GalleryPresenter(displayer, service, navigator);
    }

    @Test
    public void whenStartsPresenting_thenAttachesListener() {
        presenter.startPresenting();

        verify(displayer).attach(any(GalleryDisplayer.ActionListener.class));
    }

    @Test
    public void whenStartsPresenting_thenUpdatesGallery() {
        presenter.startPresenting();

        verify(service).updateGallery(any(SimpleCompletableObserver.class));
    }

    @Test
    public void whenStartsPresenting_thenLoadsGallery() {
        presenter.startPresenting();

        verify(service).loadGallery(ArgumentMatchers.<SimpleObserver<Gallery>>any());
    }

    @Test
    public void givenAlreadyPresenting_whenPhotoIsClicked_thenNavigatesToPhotoDetails() {
        givenAlreadyPresenting();

        listener.onClick(PHOTO, TRANSITABLE);

        verify(navigator).toDetailsOf(PHOTO, TRANSITABLE);
    }

    @Test
    public void givenAlreadyPresenting_whenRefreshing_thenUpdatesGallery() {
        givenAlreadyPresenting();

        listener.onForceRefresh();

        verify(service).updateGallery(any(SimpleCompletableObserver.class));
    }

    @Test
    public void givenAlreadyPresenting_whenSubscribedToUpdateGallery_thenDisplaysLoading() {
        givenAlreadyPresenting();

        updateObserver.onSubscribe();

        verify(displayer).displayLoading();
    }

    @Test
    public void givenAlreadyPresenting_whenLoadsGallery_thenDisplaysGallery() {
        givenAlreadyPresenting();

        Gallery gallery = Gallery.create(Collections.<Photo>emptyList());
        loadObserver.onNext(gallery);

        verify(displayer).display(gallery);
    }

    @Test
    public void givenAlreadyPresenting_whenFinishesUpdatingGallery_thenDisplaysLoaded() {
        givenAlreadyPresenting();

        updateObserver.onComplete();

        verify(displayer).displayLoaded();
    }

    @Test
    public void givenAlreadyPresenting_whenErrorUpdatingGallery_thenDisplaysError() {
        givenAlreadyPresenting();

        updateObserver.onError(null);

        verify(displayer).displayErrorUpdating();
    }

    @Test
    public void givenAlreadyPresenting_whenErrorLoadingGallery_thenDisplaysError() {
        givenAlreadyPresenting();

        loadObserver.onError(null);

        verify(displayer).displayErrorLoading();
    }

    @Test
    public void whenStopPresenting_thenDetachesDisplayer() {
        presenter.stopPresenting();

        verify(displayer).detach();
    }

    @Test
    public void whenStopsPresenting_thenClosesService() {
        presenter.stopPresenting();

        verify(service).close();
    }

    private void givenAlreadyPresenting() {
        presenter.startPresenting();
        ArgumentCaptor<SimpleCompletableObserver> updateObserverArgumentCaptor = ArgumentCaptor.forClass(SimpleCompletableObserver.class);
        verify(service).updateGallery(updateObserverArgumentCaptor.capture());
        updateObserver = updateObserverArgumentCaptor.getValue();
        ArgumentCaptor<SimpleObserver<Gallery>> loadObserverArgumentCaptor = ArgumentCaptor.forClass(SimpleObserver.class);
        verify(service).loadGallery(loadObserverArgumentCaptor.capture());
        loadObserver = loadObserverArgumentCaptor.getValue();
        ArgumentCaptor<GalleryDisplayer.ActionListener> listenerArgumentCaptor = ArgumentCaptor.forClass(GalleryDisplayer.ActionListener.class);
        verify(displayer).attach(listenerArgumentCaptor.capture());
        listener = listenerArgumentCaptor.getValue();
        reset(service);
    }
}
