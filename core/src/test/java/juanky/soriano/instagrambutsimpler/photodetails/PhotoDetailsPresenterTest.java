package juanky.soriano.instagrambutsimpler.photodetails;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import juanky.soriano.instagrambutsimpler.domain.Photo;
import juanky.soriano.instagrambutsimpler.domain.PhotoId;
import juanky.soriano.instagrambutsimpler.photodetails.service.PhotoService;
import juanky.soriano.instagrambutsimpler.service.LoadedListener;
import juanky.soriano.instagrambutsimpler.support.SimpleObserver;

import static juanky.soriano.instagrambutsimpler.domain.PhotoFixture.aPhoto;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

public class PhotoDetailsPresenterTest {
    private static final PhotoId PHOTO_ID = PhotoId.create(0);
    private static final Photo PHOTO = aPhoto().build();

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private PhotoDetailsDisplayer displayer;
    @Mock
    private PhotoService service;
    @Mock
    private LoadedListener<Photo> loadedListener;

    private PhotoDetailsPresenter presenter;
    private SimpleObserver<Photo> loadObserver;

    @Before
    public void setUp() {
        presenter = new PhotoDetailsPresenter(displayer, service);
    }

    @Test
    public void whenStartsPresenting_thenLoadsPhoto() {
        presenter.startPresenting(PHOTO_ID, loadedListener);

        verify(service).loadPhoto(eq(PHOTO_ID), ArgumentMatchers.<SimpleObserver<Photo>>any());
    }

    @Test
    public void givenAlreadyPresenting_whenLoadsPhoto_thenDisplaysPhoto() {
        givenAlreadyPresenting(PHOTO_ID);

        loadObserver.onNext(PHOTO);

        verify(displayer).display(PHOTO);
    }

    @Test
    public void givenAlreadyPresenting_whenLoadsPhoto_thenNotifiesPhotoLoaded() {
        givenAlreadyPresenting(PHOTO_ID);

        loadObserver.onNext(PHOTO);

        verify(loadedListener).onLoaded(PHOTO);
    }

    @Test
    public void givenAlreadyPresenting_whenErrorLoadingPhoto_thenDisplaysErrorLoading() {
        givenAlreadyPresenting(PHOTO_ID);

        loadObserver.onError(null);

        verify(displayer).displayErrorLoading();
    }

    @Test
    public void whenStopsPresenting_thenClosesService() {
        presenter.stopPresenting();

        verify(service).close();
    }

    private void givenAlreadyPresenting(PhotoId id) {
        presenter.startPresenting(id, loadedListener);
        ArgumentCaptor<SimpleObserver<Photo>> loadObserverArgumentCaptor = ArgumentCaptor.forClass(SimpleObserver.class);
        verify(service).loadPhoto(eq(id), loadObserverArgumentCaptor.capture());
        loadObserver = loadObserverArgumentCaptor.getValue();
    }
}
