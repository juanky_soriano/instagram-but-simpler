package juanky.soriano.instagrambutsimpler.photodetails.service;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import juanky.soriano.instagrambutsimpler.domain.Photo;
import juanky.soriano.instagrambutsimpler.domain.PhotoId;
import juanky.soriano.instagrambutsimpler.support.RxExecutor;
import juanky.soriano.instagrambutsimpler.support.SimpleObserver;

import static juanky.soriano.instagrambutsimpler.domain.PhotoFixture.aPhoto;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

public class PhotoServiceTest {

    private static final PhotoId PHOTO_ID = PhotoId.create(0);
    private static final Photo PHOTO = aPhoto().build();

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    private RxExecutor rxExecutor = RxExecutor.create(Schedulers.trampoline(), Schedulers.trampoline());
    @Mock
    private PhotoLoader loader;
    @Mock
    private SimpleObserver<Photo> loadObserver;

    private PhotoService service;

    @Before
    public void setUp() {
        service = new PhotoService(rxExecutor, loader);
    }

    @Test
    public void givenErrorLoadingPhoto_whenLoadingPhoto_thenInvokesOnError() {
        Throwable throwable = new Throwable();
        given(loader.loadPhoto(PHOTO_ID)).willReturn(Observable.<Photo>error(throwable));

        service.loadPhoto(PHOTO_ID, loadObserver);

        verify(loadObserver).onError(throwable);
    }

    @Test
    public void givenSuccessLoadingPhoto_whenLoadingPhoto_thenInvokesOnNextForPhotoAndOnComplete() {
        given(loader.loadPhoto(PHOTO_ID)).willReturn(Observable.just(PHOTO));

        service.loadPhoto(PHOTO_ID, loadObserver);

        InOrder inOrder = inOrder(loadObserver);
        inOrder.verify(loadObserver).onNext(PHOTO);
        inOrder.verify(loadObserver).onComplete();
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void whenClosing_thenStopsObserving() {
        RxExecutor rxExecutor = mock(RxExecutor.class);
        service = new PhotoService(rxExecutor, loader);
        service.close();

        verify(rxExecutor).stopObserving();
    }

}
