package juanky.soriano.instagrambutsimpler.newphoto;

import java.net.URI;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import juanky.soriano.instagrambutsimpler.service.CompletedListener;
import juanky.soriano.instagrambutsimpler.domain.Photo;
import juanky.soriano.instagrambutsimpler.newphoto.service.NewPhotoService;
import juanky.soriano.instagrambutsimpler.support.SimpleCompletableObserver;

import static juanky.soriano.instagrambutsimpler.domain.PhotoFixture.aPhoto;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

public class NewPhotoPresenterTest {
    private static final Photo PHOTO = aPhoto().build();
    private static final URI PHOTO_URI = PHOTO.image().imageUri();

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private NewPhotoDisplayer displayer;
    @Mock
    private NewPhotoService service;
    @Mock
    private CompletedListener completedListener;

    private NewPhotoPresenter presenter;
    private SimpleCompletableObserver storeObserver;
    private NewPhotoDisplayer.ActionListener actionListener;

    @Before
    public void setUp() {
        presenter = new NewPhotoPresenter(displayer, service);
    }

    @Test
    public void givenURI_whenStartsPresenting_thenDisplayForURI() {
        presenter.startPresenting(PHOTO_URI, completedListener);

        verify(displayer).displayFor(PHOTO_URI);
    }

    @Test
    public void givenURI_whenStartsPresenting_thenAttachesActionListener() {
        presenter.startPresenting(PHOTO_URI, completedListener);

        verify(displayer).attach(any(NewPhotoDisplayer.ActionListener.class));
    }

    @Test
    public void givenAlreadyPresenting_whenOnNewPhoto_thenStoresNewPhoto() {
        givenAlreadyPresenting(PHOTO_URI);

        actionListener.onNewPhoto(PHOTO);

        verify(service).storePhoto(eq(PHOTO), any(SimpleCompletableObserver.class));
    }

    @Test
    public void givenAlreadyPresenting_andStoringNewPhoto_whenErrorStoringNewPhoto_thenDisplaysError() {
        givenAlreadyPresenting(PHOTO_URI);
        givenStoringNewPhoto(PHOTO);

        storeObserver.onError(null);

        verify(displayer).displayErrorStoring();
    }

    @Test
    public void givenAlreadyPresenting_andStoringNewPhoto_whenStoringCompletes_thenNotifiesOnCompleted() {
        givenAlreadyPresenting(PHOTO_URI);
        givenStoringNewPhoto(PHOTO);

        storeObserver.onComplete();

        verify(completedListener).onCompleted();
    }

    @Test
    public void whenStopsPresenting_thenClosesService() {
        presenter.stopPresenting();

        verify(service).close();
    }

    @Test
    public void whenStopsPresenting_thenDetachesDisplayer() {
        presenter.stopPresenting();

        verify(displayer).detach();
    }

    private void givenAlreadyPresenting(URI photoUri) {
        presenter.startPresenting(photoUri, completedListener);
        ArgumentCaptor<NewPhotoDisplayer.ActionListener> actionListenerArgumentCaptor = ArgumentCaptor.forClass(NewPhotoDisplayer.ActionListener.class);
        verify(displayer).attach(actionListenerArgumentCaptor.capture());
        actionListener = actionListenerArgumentCaptor.getValue();
    }

    private void givenStoringNewPhoto(Photo photo) {
        actionListener.onNewPhoto(photo);
        ArgumentCaptor<SimpleCompletableObserver> observerArgumentCaptor = ArgumentCaptor.forClass(SimpleCompletableObserver.class);
        verify(service).storePhoto(eq(photo), observerArgumentCaptor.capture());
        storeObserver = observerArgumentCaptor.getValue();
    }
}
