package juanky.soriano.instagrambutsimpler.newphoto.service;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import io.reactivex.Completable;
import io.reactivex.schedulers.Schedulers;
import juanky.soriano.instagrambutsimpler.domain.Photo;
import juanky.soriano.instagrambutsimpler.support.RxExecutor;
import juanky.soriano.instagrambutsimpler.support.SimpleCompletableObserver;

import static juanky.soriano.instagrambutsimpler.domain.PhotoFixture.aPhoto;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class NewPhotoServiceTest {
    private static final Photo PHOTO = aPhoto().build();

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    private RxExecutor rxExecutor = RxExecutor.create(Schedulers.trampoline(), Schedulers.trampoline());
    @Mock
    private PhotoStorer storer;
    @Mock
    private SimpleCompletableObserver storeObserver;

    private NewPhotoService service;

    @Before
    public void setUp() {
        service = new NewPhotoService(rxExecutor, storer);
    }

    @Test
    public void givenErrorStoringPhoto_whenStoringPhoto_thenInvokesOnError() {
        Throwable throwable = new Throwable();
        given(storer.store(PHOTO)).willReturn(Completable.error(throwable));

        service.storePhoto(PHOTO, storeObserver);

        verify(storeObserver).onError(throwable);
    }

    @Test
    public void givenSuccessStoringPhoto_whenStoringPhoto_thenInvokesOnComplete() {
        given(storer.store(PHOTO)).willReturn(Completable.complete());

        service.storePhoto(PHOTO, storeObserver);

        verify(storeObserver).onComplete();
    }

    @Test
    public void whenClosing_thenStopsObserving() {
        RxExecutor rxExecutor = mock(RxExecutor.class);
        service = new NewPhotoService(rxExecutor, storer);
        service.close();

        verify(rxExecutor).stopObserving();
    }

}
