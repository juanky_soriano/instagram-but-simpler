package juanky.soriano.instagrambutsimpler.domain;

import java.net.URI;
import java.util.Date;

public class PhotoFixture {
    private PhotoId photoId = PhotoId.create(0);
    private Title title = Title.create("title");
    private Image image = Image.create(URI.create("http://www.image.com"));
    private Comment comment = Comment.create("comment");
    private Date date = new Date();

    private PhotoFixture() {
        //non-instantiable
    }

    public static PhotoFixture aPhoto() {
        return new PhotoFixture();
    }

    public Photo build() {
        return Photo.create(photoId, title, image, comment, date);
    }
}
